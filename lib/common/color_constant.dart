import 'dart:core';

import 'dart:ui';

import 'package:flutter/material.dart';

class ColorConstant {
  static Color appColor = const Color(0xff00a0e1);
  static Color appDarkColor = const Color(0xff0054a6);
  static Color appLightColor = const Color(0xff3670FD);
  static Color darkGrey = const Color(0xff818285);
  static Color grey = const Color(0xffADB8C5);
  static Color black = const Color(0xff383737);
  static Color white = const Color(0xffffffff);
  static Color lightGrey = const Color(0xffeae6e6);
  static Color yellow = const Color(0xffFE9705);

  static const MaterialColor kToDark = MaterialColor(
    0xff0054a6, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
  <int, Color>{
  50: Color(0xff0054a6), //10%
  100: Color(0xff00a0e1), //20%
  200: Color(0xff00a0e1), //30%
  300: Color(0xff00a0e1), //40%
  400: Color(0xff00a0e1), //50%
  500: Color(0xff00a0e1), //60%
  600: Color(0xff00a0e1), //70%
  700: Color(0xff00a0e1), //80%
  800: Color(0xff00a0e1), //90%
  900: Color(0xff00a0e1), //100%
  },
  );

//--------------------------------------------home ----------------------------------//

/*  static Color yellow_70 = const Color(0xfffdb768);
  static Color yellow_65 = const Color(0xfffcac4f);
  static Color yellow_60 = const Color(0xfffca036);
  static Color yellow_58 = const Color(0xfffc9721);


  static Color orange_70 = const Color(0xfff98d6e);
  static Color orange_65 = const Color(0xfff87a54);
  static Color orange_60 = const Color(0xfff7673b);
  static Color orange_58 = const Color(0xfff65a29);




  static Color blue_70 = const Color(0xff72c8f3);
  static Color blue_65 = const Color(0xff72c8f3);
  static Color blue_60 = const Color(0xff5bbff1);
  static Color blue_58 = const Color(0xff52bcf0);


  static Color purple_70 = const Color(0xffae9dfb);
  static Color purple_65 = const Color(0xff9a84fb);
  static Color purple_60 = const Color(0xff8f78fa);
  static Color purple_58 = const Color(0xff856bfa);*/

  static Color yellow_70 = const Color(0xff00a0e1);
  static Color yellow_65 = const Color(0xff00a4e6);
  static Color yellow_60 = const Color(0xff1abeff);
  static Color yellow_58 = const Color(0xff4dccff);


  static Color orange_70 = const Color(0xff00a0e1);
  static Color orange_65 = const Color(0xff00a4e6);
  static Color orange_60 = const Color(0xff1abeff);
  static Color orange_58 = const Color(0xff4dccff);

  static Color orange = const Color(0xff00a0e1);

  static Color blue_70 = const Color(0xff0054a6);
  static Color blue_65 = const Color(0xff0059b3);
  static Color blue_60 = const Color(0xff0066cc);
  static Color blue_58 = const Color(0xff1a8cff);


  static Color purple_70 = const Color(0xff0054a6);
  static Color purple_65 = const Color(0xff0059b3);
  static Color purple_60 = const Color(0xff0066cc);
  static Color purple_58 = const Color(0xff1a8cff);

  static Color purple = const Color(0xff9a84fb);

  static Color pink = const Color(0xffFFDEE8);
  static Color dark_pink = const Color(0xffFD346E);

  static Color green = const Color(0xffCCF2EE);
  static Color dark_green = const Color(0xff30C9B9);
  static Color status = const Color(0xff4BB543);
  }
