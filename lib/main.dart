import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/home/home_screen.dart';
import 'package:birla_white/ui/home/home_screen_widget.dart';
import 'package:birla_white/ui/login/login_screen.dart';
import 'package:birla_white/ui/otp/otp_screen.dart';
import 'package:birla_white/ui/products/product_category.dart';
import 'package:birla_white/ui/qrscan/qr_scan_code.dart';
import 'package:birla_white/ui/splash/splash_screen.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(kIsWeb
      ? Row(
          textDirection: TextDirection.ltr,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            /*  Expanded(
              child: Container(color: Colors.red),
            ),*/
            Expanded(
              child: DevicePreview(
                enabled: true,
                style: DevicePreviewStyle(
                  background: const BoxDecoration(color: Color(0xFFADADAD)),
                  toolBar: DevicePreviewToolBarStyle.light(
                      buttonsVisibility: const DevicePreviewButtonsVisibilityStyleData(
                          accessibility: false,
                          darkMode: false,
                          language: false,
                          rotate: false,
                          toggleFrame: false,
                          toggleKeyboard: false,
                          settings: false,
                          togglePreview: false,
                          device: false)),
                ),
                defaultDevice: Devices.android.samsungS8,
                data: const DevicePreviewData(
                  isDarkMode: false,
                  accessibleNavigation: false,
                ),
                devices: [
                  Devices.ios.iPhone11Pro,
                  Devices.android.samsungS8,
                ],
                plugins: [],
                builder: (context) => MyApp(),
              ),
            ),
          ],
        )
      : MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var device1 = Size(411.0, 711.0);
  var device2 = Size(1080.0, 2280.0);
  var device3 = Size(1440.0, 2960.0);
  var device = Size(411.0, 711.0);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorConstant.appDarkColor,
    ));

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Birla White',
      theme: ThemeData(
        primarySwatch: ColorConstant.kToDark,
        textTheme: GoogleFonts.montserratTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
     // home: SplashScreen(),
      // home: LoginScreen(),
       home: QrScanCode(),
    //  home: HomeScreenWidget(),
    );
  }
}
