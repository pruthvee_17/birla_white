class ProductCategoryModel {
  final String? name, date, reference_doctor, status;

  ProductCategoryModel({this.name, this.date, this.reference_doctor, this.status});
}

final List<Map> categoryList = [
  {
    "name": "Wall Care Putty",
    "total": "10",
  },
  {
    "name": "Bio-Shield Putty",
    "total": "20",
  },
  {
    "name": "WallSeal Water Proof Putty",
    "total": "5",
  },
  {
    "name": "White Cement",
    "total": "12",
  },
  {
    "name": "Excel Putty",
    "total": "4",
  },
  {
    "name": "Re-Paint Putty",
    "total": "10",
  },
  {
    "name": "Textura",
    "total": "10",
  },

];
