class QrCodeHistoryModel {
  final String? name, date, reference_doctor, status;

  QrCodeHistoryModel({this.name, this.date, this.reference_doctor, this.status});
}

final List<Map> details = [
  {
    "code": "DWZF8CAD",
    "status": "Success",
    "point": "10",
  },
  {
    "code": "3VKUPBHH",
    "status": "Success",
    "point": "50",
  },
  {
    "code": "GMY655UT",
    "status": "Pending",
    "point": "209",
  },
  {
    "code": "3VKUPBHH",
    "status": "Success",
    "point": "80",
  },
  {
    "code": "GMY655UT",
    "status": "Success",
    "point": "120",
  },
  {
    "code": "DWZF8CAD",
    "status": "Success",
    "point": "400",
  },
  {
    "code": "3VKUPBHH",
    "status": "Success",
    "point": "86",
  },
  {
    "code": "GMY655UT",
    "status": "Success",
    "point": "25",
  },
  {
    "code": "3VKUPBHH",
    "status": "Success",
    "point": "966",
  },
  {
    "code": "GMY655UT",
    "status": "Success",
    "point": "87",
  },
];
