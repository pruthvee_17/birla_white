import 'dart:io';

import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ContactUsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ContactUsScreenState();
  }
}

class ContactUsScreenState extends State<ContactUsScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController mobileNumberController = TextEditingController();
  final TextEditingController projectController = TextEditingController();
  final TextEditingController fullNameController = TextEditingController();
  final TextEditingController professionController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController queryController = TextEditingController();
  final TextEditingController contractorNameController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController pincodeController = TextEditingController();

  final FocusNode mobileNumberFocus = FocusNode();
  final FocusNode projectFocus = FocusNode();
  final FocusNode fullNameFocus = FocusNode();
  final FocusNode professionFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();
  final FocusNode queryFocus = FocusNode();
  final FocusNode noOfPainterFocus = FocusNode();
  final FocusNode pincodeFocus = FocusNode();
  final FocusNode cityFocus = FocusNode();

  bool isError = false;
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    mobileNumberFocus.addListener(() {
      setState(() {});
    });
    fullNameFocus.addListener(() {
      setState(() {});
    });
    professionFocus.addListener(() {
      setState(() {});
    });
    emailFocus.addListener(() {
      setState(() {});
    });
    queryFocus.addListener(() {
      setState(() {});
    });
    cityFocus.addListener(() {
      setState(() {});
    });
    pincodeFocus.addListener(() {
      setState(() {});
    });
    addressFocus.addListener(() {
      setState(() {});
    });
    projectFocus.addListener(() {
      setState(() {});
    });
    queryFocus.addListener(() {
      setState(() {});
    });
    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.drawerContactUs, context),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
      body: Column(
        children: [
          Flexible(
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          boxShadow: [
                            Utility.commonCardShadow,
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: (){
                                  Utility.makeCall();
                                },
                                child: SizedBox(
                                  height: 100,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(top: 6.0),
                                        child: Icon(
                                          Icons.call,
                                          size: 24,
                                          color: ColorConstant.darkGrey,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child: Text(
                                          "Call Center",
                                          style: Utility.subtitleDarkGrey_12(context),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                                        child: Text(
                                          ConstantString.birla_support_call,
                                          maxLines: 2,
                                          textAlign: TextAlign.center,
                                          style: Utility.subtitleBlack_14(context),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: 60,
                              width: 1,
                              margin: EdgeInsets.only(right: 5),
                              color: ColorConstant.darkGrey,
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: (){
                                  Utility.writeEmail();
                                },
                                child: SizedBox(
                                  height: 110,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(top: 6.0),
                                        child: Icon(
                                          Icons.email,
                                          size: 24,
                                          color: ColorConstant.darkGrey,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child: Text(
                                          "Support",
                                          style: Utility.subtitleDarkGrey_12(context),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                                        child: Text(
                                          ConstantString.birla_support_email,
                                          maxLines: 2,
                                          textAlign: TextAlign.center,
                                          style: Utility.subtitleBlack_14(context),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 20.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: projectController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: projectFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_project_subject;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context, ConstantString.enter_project_subject, ConstantString.project_subject + "*", projectFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          maxLines: 3,
                          controller: queryController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: queryFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_query;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context, ConstantString.enter_query, ConstantString.your_query + "*", queryFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 20.0),
                      Container(
                        color: ColorConstant.white,
                        height: 80,
                        child: Column(
                          children: [
                            Utility.uiSizeBox(0.0, 5.0),
                            Container(
                              margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 44,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.submit, style: Utility.buttonTextStyle_14(context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                      //if (_formKey!.currentState!.validate()) {
                                      FocusScope.of(context).unfocus();

                                      Navigator.of(context).pop();
                                      // }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 5.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
