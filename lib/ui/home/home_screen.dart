import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/products/product_category.dart';
import 'package:birla_white/ui/products/products_list.dart';
import 'package:birla_white/ui/qrscan/qr_code_history.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/foundation.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class HomeScreen extends StatefulWidget {
  /*PageController pageController;
  int currentIndex;
  String title;*/

  //HomeScreen(this.pageController, this.currentIndex, this.title);
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  List<String> bannerImgList = [
    'https://www.birlawhite.com/images/products/extocare/banner.jpg',
    'https://www.birlawhite.com/storage/home-carousels/July2020/OHElsbwWsi7I3DJdZEcb.jpg',
    'https://www.birlawhite.com/storage/home-carousels/March2021/Y2DAQVYZVRyASMLUBRgJ.jpg',
    'https://www.birlawhite.com/storage/home-carousels/July2020/OHElsbwWsi7I3DJdZEcb.jpg',
  ];

  double iconSize = 18;
  var width;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      width = 360;
    } else {
      width = MediaQuery.of(context).size.width;
    }
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
          body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          color: Colors.white,
          margin: const EdgeInsets.all(4.0),
          width: width,
          child: Column(
            children: [
              /* Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Aditya Birla White, India\'s Largest exporter, and manufacturer of White Cement, Wall Care Putty, Levelplast, GRC, Wall Primer, Textura, Re-paint Putty. Birla White gives you complete wall care solution for all your needs.',
                  textAlign: TextAlign.justify,
                ),
              ),*/
              ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                child: CarouselSlider(
                  items: bannerImgList.map((image) {
                    return Container(
                      margin: const EdgeInsets.all(5.0),
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5.0)),
                        child: CachedNetworkImage(
                          imageUrl: image,
                          fit: BoxFit.cover,
                          placeholder: (context, url) {
                            return Image.asset(
                              "assets/images/app_logo.png",
                            );
                          },
                          errorWidget: (context, url, error) {
                            return Image.asset(
                              "assets/images/app_logo.png",
                            );
                          },
                        ),
                      ),
                    );
                  }).toList(),
                  options: CarouselOptions(
                    height: width * 0.65,
                    viewportFraction: 1.0,
                    initialPage: 0,
                    enableInfiniteScroll: true,
                    autoPlay: true,
                    autoPlayInterval: const Duration(seconds: 3),
                    autoPlayAnimationDuration:
                        const Duration(milliseconds: 1000),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,
                  ),
                ),
              ),
              Utility.uiSizeBox(0.0, 10.0),
              cardData(),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          InkWell(
                            onTap: () {
                             /* Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) {
                                  return QrCodeHistory();
                                },
                              ));*/
                            },
                            child: Card(
                              elevation: 10,
                              shadowColor: ColorConstant.orange_58,
                              child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  child: Container(
                                    height: 140,
                                    //height: MediaQuery.of(context).size.height * 0.2,
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomLeft,
                                      stops: const [
                                        0.1,
                                        0.4,
                                        0.6,
                                        0.9,
                                      ],
                                      colors: [
                                        ColorConstant.orange_70,
                                        ColorConstant.orange_65,
                                        ColorConstant.orange_60,
                                        ColorConstant.orange_58,
                                      ],
                                    )),
                                    child: Container(
                                      margin: const EdgeInsets.only(
                                          top: 10, right: 10, bottom: 10),
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(ConstantString.your_points,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                      color:
                                                          ColorConstant.white,
                                                      fontSize: 15)),
                                          Flexible(
                                            child: Align(
                                              alignment: Alignment.bottomCenter,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    "5000",
                                                    maxLines: 3,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle1!
                                                        .copyWith(
                                                            color: ColorConstant
                                                                .white,
                                                            fontSize: 30),
                                                  ),
                                                  Expanded(
                                                    child: Align(
                                                      alignment:
                                                          Alignment.bottomRight,
                                                      child: Image.asset(
                                                        "assets/images/coins.png",
                                                        color:
                                                            ColorConstant.white,
                                                        height: 26,
                                                        width: 26,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                          Utility.uiSizeBox(0.0, 15.0),
                          InkWell(
                            onTap: () {
                              //widget.pageController.jumpToPage(2);
                            },
                            child: Card(
                              elevation: 10,
                              shadowColor: ColorConstant.purple_58,
                              child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  child: Container(
                                    height: 80,
                                    //height: MediaQuery.of(context).size.height * 0.12,
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomLeft,
                                      stops: const [
                                        0.1,
                                        0.4,
                                        0.6,
                                        0.9,
                                      ],
                                      colors: [
                                        ColorConstant.purple_70,
                                        ColorConstant.purple_65,
                                        ColorConstant.purple_60,
                                        ColorConstant.purple_58,
                                      ],
                                    )),
                                    child: Container(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(ConstantString.redeem,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                      color:
                                                          ColorConstant.white,
                                                      fontSize: 15)),
                                          Flexible(
                                              child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Icon(
                                              Icons.card_giftcard,
                                              color: ColorConstant.white,
                                              size: 26,
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Utility.uiSizeBox(10.0, 0.0),
                    Expanded(
                      child: Column(
                        children: [
                          InkWell(
                            onTap: () {

                            },
                            child: Card(
                              elevation: 10,
                              shadowColor: ColorConstant.blue_58,
                              child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  child: Container(
                                    height: 80,
                                    //height: MediaQuery.of(context).size.height * 0.12,
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      stops: const [
                                        0.1,
                                        0.4,
                                        0.6,
                                        0.9,
                                      ],
                                      colors: [
                                        ColorConstant.blue_70,
                                        ColorConstant.blue_65,
                                        ColorConstant.blue_60,
                                        ColorConstant.blue_58,
                                      ],
                                    )),
                                    child: Container(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(ConstantString.scan,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                      color:
                                                          ColorConstant.white,
                                                      fontSize: 15)),
                                          Flexible(
                                              child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Icon(
                                              Icons.qr_code,
                                              color: ColorConstant.white,
                                              size: 26,
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                          Utility.uiSizeBox(0.0, 15.0),
                          InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) {
                                  return ProductsList();
                                },
                              ));
                            },
                            child: Card(
                              elevation: 10,
                              shadowColor: ColorConstant.yellow_58,
                              child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  child: Container(
                                    height: 140,
                                    //height: MediaQuery.of(context).size.height * 0.2,
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      stops: const [
                                        0.1,
                                        0.4,
                                        0.6,
                                        0.9,
                                      ],
                                      colors: [
                                        ColorConstant.yellow_70,
                                        ColorConstant.yellow_65,
                                        ColorConstant.yellow_60,
                                        ColorConstant.yellow_58,
                                      ],
                                    )),
                                    child: Container(
                                      margin: const EdgeInsets.only(top: 10),
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(ConstantString.products,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                          color: ColorConstant
                                                              .white,
                                                          fontSize: 15)),
                                              Flexible(
                                                  child: Align(
                                                alignment: Alignment.topRight,
                                                child: Icon(
                                                  Icons.format_paint,
                                                  color: ColorConstant.white,
                                                  size: 26,
                                                ),
                                              ))
                                            ],
                                          ),
                                          Flexible(
                                              child: Container(
                                            margin: const EdgeInsets.only(
                                                top: 10, right: 10),
                                            child: Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  "14",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                          fontSize: 50,
                                                          color: ColorConstant
                                                              .white),
                                                )),
                                          )),
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Utility.uiSizeBox(0.0, 5.0),
            ],
          ),
        ),
      )),
    );
  }

  cardData() {
    return Container(
      height: 160,
      margin: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 15),
      decoration: BoxDecoration(
        boxShadow: [
          Utility.commonCardShadow,
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Container(
        padding: const EdgeInsets.only(
          top: 20,
          left: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //--------------------------Profile Completion------------------//
            Text(ConstantString.profile_completion,
                style: Utility.subtitleBlack_18(context)),

            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(ConstantString.address,
                                        style: Utility.subtitleAppDarkColor_15(
                                            context)),
                                    Container(
                                      margin: const EdgeInsets.only(top: 2),
                                      child: Text(ConstantString.pending,
                                          style: Utility.subtitleYellow_10(
                                              context)),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(ConstantString.misc_details,
                                        style: Utility.subtitleAppDarkColor_15(
                                            context)),
                                    Container(
                                      margin: const EdgeInsets.only(top: 2),
                                      child: Text(ConstantString.pending,
                                          style: Utility.subtitleYellow_10(
                                              context)),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 18),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(ConstantString.bank_details,
                                        style: Utility.subtitleAppDarkColor_15(
                                            context)),
                                    Container(
                                      margin: const EdgeInsets.only(top: 2),
                                      child: Text(ConstantString.pending,
                                          style: Utility.subtitleYellow_10(
                                              context)),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      )),
                ),
                Expanded(
                  flex: 2,
                  child: CircularPercentIndicator(
                    radius: 70.0,
                    animation: true,
                    animationDuration: 1200,
                    lineWidth: 4.7,
                    percent: 0.25,
                    center: const Text(
                      "25%",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 12.0),
                    ),
                    circularStrokeCap: CircularStrokeCap.butt,
                    progressColor: ColorConstant.appDarkColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
