import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/contactus/contact_us_screen.dart';
import 'package:birla_white/ui/home/home_screen.dart';
import 'package:birla_white/ui/login/login_screen.dart';
import 'package:birla_white/ui/notification/notification_history.dart';
import 'package:birla_white/ui/privacypolicy/privacy_policy.dart';
import 'package:birla_white/ui/products/product_category.dart';
import 'package:birla_white/ui/products/products_list.dart';
import 'package:birla_white/ui/qrscan/qr_scan_code.dart';
import 'package:birla_white/ui/redeem/redeem.dart';
import 'package:birla_white/ui/register/profile_info_screen.dart';
import 'package:birla_white/ui/setting/edit_profile_screen.dart';
import 'package:birla_white/ui/setting/setting.dart';
import 'package:birla_white/ui/storelocator/store_locator_screen.dart';
import 'package:birla_white/ui/termcondition/term_condition.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';

class HomeScreenWidget extends StatefulWidget {
  String Tag = 'HomeScreen';
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    // TODO: implement createState
    return HomeScreenWidgetState();
  }
}

class HomeScreenWidgetState extends State<HomeScreenWidget> {
  int _currentIndex = 0;
  String title = ConstantString.navHome;
  PageController? _pageController;
  double iconSize = 24.0;
  bool doubleBackToExitPressedOnce = false;
  DateTime? currentBackPressTime;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  void showHideDrawer() {
  //  Utility.printLog(widget.Tag, drawerController!.value.visible.toString());

    if (widget._scaffoldKey.currentState!.isDrawerOpen) {
      widget._scaffoldKey.currentState!.openEndDrawer();
    } else {
      widget._scaffoldKey.currentState!.openDrawer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.sync(onWillPop),
      child: Scaffold(
        key: widget._scaffoldKey,
        appBar: AppBar(
          title: Text(
            title,
            style: Utility.subtitleWhite_16(context),
          ),
          centerTitle: true,
          actions: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return EditProfileInfoScreen();
                  },
                ));
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: Image.asset(
                  "assets/images/user.png",
                  height: 25,
                  width: 25,
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomNavyBar(
          selectedIndex: _currentIndex,
          onItemSelected: (index) {
            setState(() {
              _currentIndex = index;
            });
            _pageController!.jumpToPage(index);
          },
          items: <BottomNavyBarItem>[
            BottomNavyBarItem(
              title: Text(
                ConstantString.navHome,
                style: Utility.subtitleAppColor_14(context),
              ),
              icon: const Icon(Icons.home),
              activeColor: ColorConstant.appColor,
              inactiveColor: ColorConstant.appDarkColor,
              textAlign: TextAlign.center,
            ),
            BottomNavyBarItem(
              title: Text(
                ConstantString.navScan,
                style: Utility.subtitleAppColor_14(context),
              ),
              icon: const Icon(Icons.qr_code),
              activeColor: ColorConstant.appColor,
              inactiveColor: ColorConstant.appDarkColor,
              textAlign: TextAlign.center,
            ),
            BottomNavyBarItem(
              title: Text(
                ConstantString.navRedeem,
                style: Utility.subtitleAppColor_14(context),
              ),
              icon: const Icon(Icons.card_giftcard),
              activeColor: ColorConstant.appColor,
              inactiveColor: ColorConstant.appDarkColor,
              textAlign: TextAlign.center,
            ),
            BottomNavyBarItem(
              title: Text(
                ConstantString.navSetting,
                style: Utility.subtitleAppColor_14(context),
              ),
              icon: const Icon(Icons.settings),
              activeColor: ColorConstant.appColor,
              inactiveColor: ColorConstant.appDarkColor,
              textAlign: TextAlign.center,
            ),
          ],
        ),
        body: SizedBox.expand(
          child: PageView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _pageController,
              onPageChanged: (index) {
                if (index == 0) {
                  title = ConstantString.navHome;
                } else if (index == 1) {
                  title = ConstantString.navScan;
                } else if (index == 2) {
                  title = ConstantString.navRedeem;
                } else {
                  title = ConstantString.navSetting;
                }
              },
              children: [HomeScreen(), QrScanCode(), Redeem(), Setting()]),
        ),
        drawer: Drawer(
          child: Container(
            color: ColorConstant.white,
            child: ListTileTheme(
              selectedColor: ColorConstant.appColor,
              textColor: Colors.white,
              iconColor: Colors.black,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _createHeader(),
                  ListTile(
                    onTap: () {
                      showHideDrawer();
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return ProductsList();
                        },
                      ));
                    },
                    leading: Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.format_paint_outlined,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.participating_products,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      showHideDrawer();
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return StoreLocatorScreen();
                        },
                      ));
                    },
                    leading: Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.location_on_outlined,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.drawerStoreLocator,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      showHideDrawer();
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return TermConditionScreen();
                        },
                      ));
                    },
                    leading: Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.note_alt_outlined,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.term_condition,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      showHideDrawer();
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return PrivacyPolicyScreen();
                        },
                      ));
                    },
                    leading: Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.privacy_tip_outlined,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.privacy_policy,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      showHideDrawer();
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return ContactUsScreen();
                        },
                      ));
                    },
                    leading: Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.contact_phone_outlined,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.drawerContactUs,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                       ListTile(
                    onTap: () {
                      showHideDrawer();
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return NotificationHistory();
                        },
                      ));
                    },
                    leading: Container(
                      margin: const EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.notifications,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.drawerNotification,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      showHideDrawer();
                      //showLogoutDialog();

                    },
                    leading: Container(
                      margin: const EdgeInsets.only(left: 10.0),
                      child: Icon(
                        Icons.logout,
                        size: iconSize,
                      ),
                    ),
                    title: Text(
                      ConstantString.drawerLogout,
                      style: Utility.subtitleBlack_16(context),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        child: Stack(children: <Widget>[
          Center(child: Image.asset("assets/images/app_logo.png")),
        ]));
  }

  Widget _createDrawerItem({IconData? icon, String? text, GestureTapCallback? onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text!),
          )
        ],
      ),
      onTap: onTap,
    );
  }

  bool onWillPop() {
    if (_pageController!.page!.round() == _pageController!.initialPage) {
      DateTime now = DateTime.now();
      if (currentBackPressTime == null || now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
        currentBackPressTime = now;
        final snackBar = SnackBar(
          content: Text(ConstantString.navBackMsg),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        return false;
      }
      return true;
    } else {
      setState(() {
        _currentIndex = 0;
        title = ConstantString.navHome;
        _pageController!.jumpToPage(0);
      });
      return false;
    }
  }

  @override
  bool get wantKeepAlive {
    return true;
  }

  void showLogoutDialog() {
    var dialog = CustomAlertDialog(
        title: "Logout",
        message: "Are you sure, do you want to logout?",
        onPostivePressed: () {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        },
        onNegativePressed: () {
          Navigator.of(context).pop();
        },
        positiveBtnText: 'Yes',
        negativeBtnText: 'No');
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }
}

class CustomAlertDialog extends StatelessWidget {
  final Color? bgColor;
  final String? title;
  final String? message;
  final String? positiveBtnText;
  final String? negativeBtnText;
  final Function? onPostivePressed;
  final Function? onNegativePressed;
  final double? circularBorderRadius;

  CustomAlertDialog({
    this.title,
    this.message,
    this.circularBorderRadius = 15.0,
    this.bgColor = Colors.white,
    this.positiveBtnText,
    this.negativeBtnText,
    this.onPostivePressed,
    this.onNegativePressed,
  })  : assert(bgColor != null),
        assert(circularBorderRadius != null);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title != null ? Text(title!) : null,
      content: message != null ? Text(message!) : null,
      backgroundColor: bgColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(circularBorderRadius!)),
      actions: <Widget>[
        if (negativeBtnText != null)
          FlatButton(
            child: Text(negativeBtnText!),
            textColor: Theme.of(context).accentColor,
            onPressed: () {
              Navigator.of(context).pop();
              if (onNegativePressed != null) {
                onNegativePressed!();
              }
            },
          ),
        if (positiveBtnText != null)
          FlatButton(
            child: Text(positiveBtnText!),
            textColor: Theme.of(context).accentColor,
            onPressed: () {
              if (onPostivePressed != null) {
                onPostivePressed!();
              }
            },
          ),
      ],
    );
  }
}
