import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/model/qr_code_history_model.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';

class NotificationHistory extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NotificationHistoryState();
  }
}

class NotificationHistoryState extends State<NotificationHistory> {
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: Utility.commonAppBar(ConstantString.notification_history, context),
      body: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 6.0,
        ),
        height: MediaQuery.of(context).size.height,
        color: ColorConstant.white,
        width: double.infinity,
        child: ListView.separated(
          itemCount: details.length,
          itemBuilder: (BuildContext context, int index) {
            return notificationList(context, index);
          },
          separatorBuilder: (BuildContext context, int index) {
            return Container(color: ColorConstant.grey.withOpacity(0.4), height: 1);
          },
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget notificationList(BuildContext context, int index) {
    //DateTime time1 = DateTime.parse("${notificationController.notificationList[index].date} ${notificationController.notificationList[index].time}");

    print("Index: " + index.toString());
    return Container(
      padding: const EdgeInsets.only(top: 6, bottom: 6),
      decoration: BoxDecoration(
        color: (index % 3 == 0) ? ColorConstant.white : ColorConstant.appDarkColor.withOpacity(0.1),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: Image.network(
                "https://picsum.photos/200/300",
                fit: BoxFit.cover,
                height: 45,
                width: 45,
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Padding(
                padding: const EdgeInsets.all(0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0),
                          child: Text(
                            "Test Notification Title",
                            maxLines: 4,
                            style: Utility.subtitleBoldBlack_14(context),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Text(
                            "Test Notification Description Test Notification DescriptionTest Notification DescriptionTest Notification DescriptionTest Notification DescriptionTest Notification Description",
                            style: Utility.subtitleBlack_10(context),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, right: 8.0),
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          "5 min ago",
                          style: Utility.subtitleBoldDarKGrey_11(context),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
