import 'dart:async';

import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/login/login_screen.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OtpScreen extends StatefulWidget {

  OtpScreen();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OtpScreenState();
  }
}

class OtpScreenState extends State<OtpScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();
  final TextEditingController mobileNoController = TextEditingController();
  final FocusNode _mobileNoFocus = FocusNode();
  bool isError = false;
  final _verificationFormKey = GlobalKey<FormState>();
  String otp = "";
  Timer? _timer;
  int _start = 60;
  int count=1;
  bool isEnable = false;
  final TextEditingController otp1Controller = TextEditingController();
  final TextEditingController otp2Controller = TextEditingController();
  final TextEditingController otp3Controller = TextEditingController();
  final TextEditingController otp4Controller = TextEditingController();
  final TextEditingController otp5Controller = TextEditingController();
  final TextEditingController otp6Controller = TextEditingController();
  final FocusNode otp1Focus =  FocusNode();
  final FocusNode otp2Focus =  FocusNode();
  final FocusNode otp3Focus =  FocusNode();
  final FocusNode otp4Focus =  FocusNode();
  final FocusNode otp5Focus =  FocusNode();
  final FocusNode otp6Focus =  FocusNode();


  String _smsCode = "";
  bool isListening = false;
  getCode(String sms) {
    if (sms != null) {
      final intRegex = RegExp(r'\d+', multiLine: true);
      final code = intRegex.allMatches(sms).first.group(0);
      print("OTP:- $code");
      return code;
    }
    return "NO SMS";
  }
  void startTimer() async {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (_start == 0) {
          setState(() {
            isEnable = true;
            timer.cancel();
            _start = 60;
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
    // Future.delayed(Duration(seconds: 5)).then((value) => readingOtp());
  }
/*

  void readingOtp() async {
    String smsCode = await SmsRetriever.startListening();
    _smsCode = getCode(smsCode);
    isListening = false;
    setState(() {
      otp1Controller.text = _smsCode[0];
      otp2Controller.text = _smsCode[1];
      otp3Controller.text = _smsCode[2];
      otp4Controller.text = _smsCode[3];
      otp5Controller.text = _smsCode[4];
      otp6Controller.text = _smsCode[5];
    });
    if(_smsCode.length == 6) {
      await otpController.getVerifyOtp(_smsCode, widget.verificationId, widget.phone, widget.firstName, widget.lastName, widget.email,widget.mrCode,"manual",null);
    }
    print("New Code:- $_smsCode");
    SmsRetriever.stopListening();
  }



  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
*/

  @override
  void initState() {
    // TODO: implement initState
    otp1Focus.addListener(() {
      otp1Controller.selection =  TextSelection(
        baseOffset: 0,
        extentOffset: otp1Controller.text.length,
      );
    });
    otp2Focus.addListener(() {
      otp2Controller.selection =  TextSelection(
        baseOffset: 0,
        extentOffset: otp2Controller.text.length,
      );
    });
    otp3Focus.addListener(() {
      otp3Controller.selection =  TextSelection(
        baseOffset: 0,
        extentOffset: otp3Controller.text.length,
      );
    });
    otp4Focus.addListener(() {
      otp4Controller.selection =  TextSelection(
        baseOffset: 0,
        extentOffset: otp4Controller.text.length,
      );
    });
    otp5Focus.addListener(() {
      otp5Controller.selection =  TextSelection(
        baseOffset: 0,
        extentOffset: otp5Controller.text.length,
      );
    });
    otp6Focus.addListener(() {
      otp6Controller.selection =  TextSelection(
        baseOffset: 0,
        extentOffset: otp6Controller.text.length,
      );
    });
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.white,
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: Container(
        color: ColorConstant.white,
        height: 145,
        child: Column(
          children: [
            Utility.uiSizeBox(0.0, 5.0),
            Container(
              margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 10),
              width: MediaQuery.of(context).size.width,
              height: 44,
              child: TextButton(
                style: Utility.buttonStyle(context),
                onPressed: () async {
                  if (_formKey!.currentState!.validate()) {
                    FocusScope.of(context).unfocus();
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
                  } else {
                    print("Error");
                  }
                },
                child: Text(ConstantString.confirm, style: Utility.buttonTextStyle_18(context)),
              ),
            ),
            Utility.uiSizeBox(0.0, 30.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  ConstantString.dont_receive_the_otp,
                  style: Utility.subtitleGrey_14(context),
                ),
                Text(
                  ConstantString.resend_otp,
                  style: Utility.subtitleAppColor_14(context),
                ),
              ],
            ),
            Utility.uiSizeBox(0.0, 5.0),
          ],
        ),
      ),
      body: Column(
        children: [
          //------------------------------OTP-----------------------------//
            Center(
              child: Container(
                  margin: EdgeInsets.only(top: 50.0, bottom: 50.0, left: 10.0, right: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        // height: 50.0,
                        width: 40.0,
                        child: Container(
                          child: Center(
                            child: TextFormField(
                              controller: otp1Controller,
                              keyboardType: TextInputType.number,
                              focusNode: otp1Focus,
                              showCursor: true,
                              textInputAction: TextInputAction.next,
                              textAlign: TextAlign.center,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.appDarkColor),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  contentPadding: EdgeInsets.zero
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1),
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              style: Utility.subtitleBlack_14(context),

                              onTap: () {
                                setState(() {
                                  otp1Focus.context;
                                });
                                otp1Controller.selection = new TextSelection(
                                  baseOffset: 0,
                                  extentOffset: otp1Controller.text.length,
                                );
                              },
                              onChanged: (text) {
                                if(text.length == 1) {
                                  setState(() {
                                    _fieldFocusChange(context, otp1Focus, otp2Focus);
                                  });
                                }
                              },
                              validator: (value) {
                                if(value!.isEmpty) {
                                  Utility.showToast("Please enter valid otp !");
                                }
                                return null;
                              },
                              // maxLength: 1,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 40.0,
                        child: Container(
                          child: Center(
                            child: TextFormField(
                              controller: otp2Controller,
                              keyboardType: TextInputType.number,
                              focusNode: otp2Focus,
                              textInputAction: TextInputAction.next,
                              textAlign: TextAlign.center,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  contentPadding: EdgeInsets.zero
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1),
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              style: Utility.subtitleBlack_14(context),
                              onTap: () {
                                setState(() {
                                  otp2Focus.context;
                                });
                                otp2Controller.selection = new TextSelection(
                                  baseOffset: 0,
                                  extentOffset: otp2Controller.text.length,
                                );
                              },
                              onChanged: (text) {
                                if(text.length == 1) {
                                  setState(() {
                                    _fieldFocusChange(context, otp2Focus, otp3Focus);
                                  });
                                }
                                if(text.isEmpty) {
                                  FocusScope.of(context).requestFocus(otp1Focus);
                                }

                              },
                              validator: (value) {
                                if(value!.isEmpty) {
                                  Utility.showToast("Please enter valid otp !");
                                }
                                return null;
                              },
                              // maxLength: 1,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 40.0,
                        child: Container(
                          child: Center(
                            child: TextFormField(
                              controller: otp3Controller,
                              keyboardType: TextInputType.number,
                              focusNode: otp3Focus,
                              textInputAction: TextInputAction.next,
                              textAlign: TextAlign.center,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  contentPadding: EdgeInsets.zero
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1),
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              style: Utility.subtitleBlack_14(context),
                              onTap: () {
                                setState(() {
                                  otp3Focus.context;
                                });
                                otp3Controller.selection = new TextSelection(
                                  baseOffset: 0,
                                  extentOffset: otp3Controller.text.length,
                                );
                              },
                              onChanged: (text) {
                                if(text.length == 1) {
                                  setState(() {
                                    _fieldFocusChange(context, otp3Focus, otp4Focus);
                                  });
                                }
                                if(text.isEmpty) {
                                  FocusScope.of(context).requestFocus(otp2Focus);
                                }
                              },
                              validator: (value) {
                                if(value!.isEmpty) {
                                  Utility.showToast("Please enter valid otp !");
                                }
                                return null;
                              },
                              // maxLength: 1,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 40.0,
                        child: Container(
                          child: Center(
                            child: TextFormField(
                              controller: otp4Controller,
                              keyboardType: TextInputType.number,
                              focusNode: otp4Focus,
                              textInputAction: TextInputAction.next,
                              textAlign: TextAlign.center,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  contentPadding: EdgeInsets.zero
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1),
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              style: Utility.subtitleBlack_14(context),
                              onTap: () {
                                setState(() {
                                  otp4Focus.context;
                                });
                                otp4Controller.selection = TextSelection(
                                  baseOffset: 0,
                                  extentOffset: otp4Controller.text.length,
                                );
                              },
                              onChanged: (text) {
                                if(text.length == 1) {
                                  setState(() {
                                    _fieldFocusChange(context, otp4Focus, otp5Focus);
                                  });
                                }
                                if(text.isEmpty) {
                                  FocusScope.of(context).requestFocus(otp3Focus);
                                }
                              },
                              validator: (value) {
                                if(value!.isEmpty) {
                                  Utility.showToast("Please enter valid otp !");
                                }
                                return null;
                              },
                              // maxLength: 1,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 40.0,
                        child: Container(
                          child: Center(
                            child: TextFormField(
                              controller: otp5Controller,
                              keyboardType: TextInputType.number,
                              focusNode: otp5Focus,
                              textInputAction: TextInputAction.next,
                              textAlign: TextAlign.center,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  contentPadding: EdgeInsets.zero
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1),
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              style: Utility.subtitleBlack_14(context),
                              onTap: () {
                                setState(() {
                                  otp5Focus.context;
                                });
                                otp5Controller.selection = TextSelection(
                                  baseOffset: 0,
                                  extentOffset: otp5Controller.text.length,
                                );
                              },
                              onChanged: (text) {
                                if(text.length == 1) {
                                  setState(() {
                                    _fieldFocusChange(context, otp5Focus, otp6Focus);
                                  });
                                }
                                if(text.isEmpty) {
                                  FocusScope.of(context).requestFocus(otp4Focus);
                                }
                              },
                              validator: (value) {
                                if(value!.isEmpty) {
                                  Utility.showToast("Please enter valid otp !");
                                }
                                return null;
                              },
                              // maxLength: 1,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 40.0,
                        child: Container(
                          child: Center(
                            child: TextFormField(
                              controller: otp6Controller,
                              keyboardType: TextInputType.number,
                              focusNode: otp6Focus,
                              textInputAction: TextInputAction.done,
                              textAlign: TextAlign.center,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(color: ColorConstant.darkGrey),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  contentPadding: EdgeInsets.zero
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1),
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              style: Utility.subtitleBlack_14(context),
                              onTap: () {
                                setState(() {
                                  otp6Focus.context;
                                });
                                otp6Controller.selection =  TextSelection(
                                  baseOffset: 0,
                                  extentOffset: otp6Controller.text.length,
                                );
                              },
                              onChanged: (text) {
                                if(text.length == 1) {
                                  FocusScope.of(context).requestFocus( FocusNode());
                                }
                                if(text.isEmpty) {
                                  FocusScope.of(context).requestFocus(otp5Focus);
                                }
                              },
                              validator: (value) {
                                if(value!.isEmpty) {
                                  Utility.showToast("Please enter valid otp !");
                                }
                                return null;
                              },
                              // maxLength: 1,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ),

          //------------------- Resend-------------------//

            InkWell(
              onTap: () async {
                if(isEnable) {
                  count+=1;
                  setState(() {
                    isEnable = false;
                  });
                  startTimer();
                  //signInWithPhone(widget.phone);
                }
              },
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Text(
                 'Resend OTP',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: isEnable ? ColorConstant.appColor : ColorConstant.darkGrey
                  ),
                ),
              ),
            ),
          //--------------resend after few second ---------------//

              !isEnable ? Text(("after") + " $_start " +("seconds"),
                style: TextStyle(
                  color:ColorConstant.darkGrey,
                  fontSize: 15.0,
                ),
              ) : SizedBox.shrink()

        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
