import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/products/product_category.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CategoryWiseProductsList extends StatefulWidget {
  String?  itemCount;
  String?  itemName;

  CategoryWiseProductsList({this.itemName,this.itemCount, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CategoryWiseProductsListState();
  }
}

class CategoryWiseProductsListState extends State<CategoryWiseProductsList> {
  late TextEditingController _searchQuery;
  bool _isSearching = false;
  String searchQuery = "Search query";
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _searchQuery = new TextEditingController();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  void _startSearch() {
    print("open search box");
    //  ModalRoute.of(context).addLocalHistoryEntry(new LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    print("close search box");
    setState(() {
      _searchQuery.clear();
      updateSearchQuery("Search query");
    });
  }

  Widget _buildTitle(BuildContext context) {
//  var horizontalTitleAlignment = Platform.isIOS ? CrossAxisAlignment.center : CrossAxisAlignment.start;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(widget.itemName.toString()),
        ],
      ),
    );
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQuery,
      autofocus: true,
      decoration: const InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: const TextStyle(color: Colors.white),
      ),
      cursorColor: ColorConstant.white,
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: updateSearchQuery,
    );
  }

  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
    print("search query " + newQuery);
  }

  List<Widget> _buildActions() {
    if (_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            if (_searchQuery == null || _searchQuery.text.isEmpty) {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => CategoryWiseProductsList(itemName:"Wall Care Putty",itemCount:"10")),
              );
              return;
            }
            _clearSearchQuery();
          },
        ),
      ];
    }

    return <Widget>[
      Row(
        children: [
          Padding(
            padding: EdgeInsets.only(right: 10.0, top: 5, bottom: 5),
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: Icon(
                Icons.search,
                color: ColorConstant.white,
                size: 24,
              ),
              onPressed: _startSearch,
            ),
          ),
        ],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //appBar: Utility.commonAppBar(widget.itemName.toString(), context),
      appBar: AppBar(
        backgroundColor: ColorConstant.appDarkColor,
        automaticallyImplyLeading: _isSearching ? false : false,
        titleSpacing: 0,
        centerTitle: true,
        leading: _isSearching
            ? BackButton(
          onPressed: () => {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => CategoryWiseProductsList()),
            )
          },
        )
            : Container(),
        title: _isSearching ? _buildSearchField() : _buildTitle(context),
        actions: _buildActions(),
      ),
      body: Container(color: Colors.white, child: productsListWidget(context)),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget productsListWidget(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: int.parse(widget.itemCount.toString()),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.only(left: 8, top: 10, right: 8, bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10), bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
            boxShadow: [
              Utility.commonCardShadow,
            ],
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(10.0), topLeft: Radius.circular(10.0)),
                child: CachedNetworkImage(
                  imageUrl: "https://www.birlawhite.com/images/products/bio-shield/available-sizes/sku.jpg",
                  fit: BoxFit.cover,
                  height: 100,
                  width: 100,
                  placeholder: (context, url) {
                    return Image.asset(
                      "assets/images/app_logo.png",
                    );
                  },
                  errorWidget: (context, url, error) {
                    return Image.asset(
                      "assets/images/app_logo.png",
                    );
                  },
                ),
              ),
              Flexible(
                child: Container(
                  height: 90,
                  alignment: Alignment.topLeft,
                  // color: Colors.yellow,
                  padding: const EdgeInsets.only(
                    left: 10,
                    right: 4.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Boi-Shield Putty", style: Utility.subtitleBoldBlack_14(context)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("30 Kg", maxLines: 2, overflow: TextOverflow.ellipsis, style: Utility.subtitleDarkGrey_12(context)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("CFTY45", maxLines: 2, overflow: TextOverflow.ellipsis, style: Utility.subtitleDarkGrey_12(context)),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                // color: Colors.red,
                padding: const EdgeInsets.only(right: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        // color: ColorConstant.green,
                        border: Border.all(color: ColorConstant.appDarkColor, width: 1.0),
                      ),
                      height: 40,
                      width: 40,
                      child: Center(
                        child: Text("300", style: Utility.subtitleAppDarkColor_14(context)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 4.0,
                      ),
                      child: Text("Points", style: Utility.subtitleAppDarkColor_12(context)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
