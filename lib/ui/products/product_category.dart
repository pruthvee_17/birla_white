import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/model/product/product_category_model.dart';
import 'package:birla_white/ui/products/products_list.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'category_wise_products_list.dart';

class ProductCategory extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductCategoryState();
  }
}

class ProductCategoryState extends State<ProductCategory> {
  var height;
  var width;
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }
  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      height = 740;
      width = 360;
    } else {
      width = MediaQuery.of(context).size.width;
      height = MediaQuery.of(context).size.height;
    }
    // TODO: implement build
    return Scaffold(
      appBar: Utility.commonAppBar(ConstantString.category, context),
      body: Container(
        padding: const EdgeInsets.only(left: 4, right: 4),
        color: Colors.white,
        child: ListView.builder(
          shrinkWrap: false,
          scrollDirection: Axis.vertical,
          itemCount: categoryList.length,
          itemBuilder: (context, index) {
            return Container(
              margin: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10), topRight: Radius.circular(10), bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                boxShadow: [
                  Utility.commonCardShadow,
                ],
              ),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return CategoryWiseProductsList(itemName:categoryList[index]["name"] ,itemCount: categoryList[index]["total"]);
                      },
                    ),
                  );
                },
                child: Container(
                  //padding: EdgeInsets.all(10.0),
                  margin:  const EdgeInsets.symmetric(horizontal: 14.0,vertical: 22),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        categoryList[index]["name"]  ,
                        textAlign: TextAlign.start,
                        style: Utility.headlineBlack_17(context),
                      ),
                      Text(
                        " (" + categoryList[index]["total"] + ")",
                        style: Utility.subtitleAppDarkColor_17(context),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
