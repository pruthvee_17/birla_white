import 'package:birla_white/ui/qrscan/qr_code_history.dart';
import 'package:birla_white/ui/qrscan/qr_view_screen.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class QrScanCode extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return QrScanCodeState();
  }
}

class QrScanCodeState extends State<QrScanCode> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController couponController = TextEditingController();
  final FocusNode couponNoFocus = FocusNode();
  final TextEditingController mobileNoController = TextEditingController();
  final FocusNode _mobileNoFocus = FocusNode();
  bool isError = false;
  List<String> result=[];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SingleChildScrollView(
          child: Container(
        alignment: Alignment.topCenter,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Utility.uiSizeBox(0.0, 30.0),
              Text(
                ConstantString.scan_code,
                textAlign: TextAlign.center,
                style: Utility.subtitleAppColorBold_24(context),
              ),
              Utility.uiSizeBox(0.0, 50.0),
              InkWell(
                onTap: () {
                  goToSecondScreen(context);
                },
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    //color: ColorConstant.appColor,
                  ),
                  child: Image.asset(
                    "assets/images/qr_code.png",
                    height: 100,
                    width: 100,
                    //color: ColorConstant.dark_pink,
                  ),
                ),
              ),
              Utility.uiSizeBox(0.0, 50.0),
              Text(
                ConstantString.hold_your_phone,
                textAlign: TextAlign.center,
                style: Utility.subtitleBlack_14(context),
              ),
              Utility.uiSizeBox(0.0, 30.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: TextFormField(
                  autofocus: false,
                  showCursor: true,
                  controller: couponController,
                  onChanged: (value) {
                    if (isError) {
                      _formKey?.currentState!.validate();
                    }
                  },
                  focusNode: couponNoFocus,
                  validator: (val) {
                    if (val == null || val.isEmpty) {
                      isError = true;
                      return ConstantString.enter_coupon_code;
                    }
                    isError = false;
                  },
                  maxLines: 5,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  style: Utility.subtitleBlack_14(context),
                  decoration: Utility.editBoxInputDecoration(
                    context,
                    ConstantString.enter_coupon_code,
                    ConstantString.coupon_code,
                    couponNoFocus.hasFocus,
                    iconButton: IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                          couponController.clear();
                          result.clear();
                        }),
                  ),
                ),
              ),
              Utility.uiSizeBox(0.0, 15.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: TextFormField(
                  autofocus: false,
                  showCursor: true,
                  controller: mobileNoController,
                  onChanged: (value) {
                    if (isError) {
                      _formKey?.currentState!.validate();
                    }
                  },
                  focusNode: _mobileNoFocus,
                  validator: (val) {
                    /*if (val == null || val.isEmpty) {
                      isError = true;
                      return ConstantString.enter_mobile_no;
                    } else if (val.length != 10) {
                      isError = true;
                      return ConstantString.enter_valid_mobile_no;
                    }
                    isError = false;*/
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  ],
                  textInputAction: TextInputAction.next,
                  maxLength: 10,
                  style: Utility.subtitleBlack_14(context),
                  decoration: Utility.editBoxInputDecoration(context, ConstantString.enter_retailer_mobile_number, ConstantString.mapped_retailer, couponNoFocus.hasFocus),
                ),
              ),
              Utility.uiSizeBox(0.0, 30.0),
              Container(
                margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 10),
                width: MediaQuery.of(context).size.width,
                height: 44,
                child: TextButton(
                  style: Utility.buttonStyle(context),
                  onPressed: () async {
                    if (_formKey!.currentState!.validate()) {
                      FocusScope.of(context).unfocus();
                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => QrCodeHistory()));
                    }
                  },
                  child: Text(ConstantString.submit, style: Utility.buttonTextStyle_14(context)),
                ),
              ),
              Utility.uiSizeBox(0.0, 30.0),
            ],
          ),
        ),
      )),
    );
  }

  void goToSecondScreen(BuildContext context) async {
     result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => new QrViewScreen(),
          fullscreenDialog: true,
        ));

    print("result : " + result.toString());
    if (result.length > 1) {
      for (int i = 0; i < result.length; i++) {
        couponController.text = couponController.text + result[i].toString()+ ",";
      }
    } else {
      couponController.text = result.toString();
    }

  }
}
