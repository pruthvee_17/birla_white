import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:url_launcher/url_launcher.dart';

class QrViewScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return QrViewScreenState();
  }
}

class QrViewScreenState extends State<QrViewScreen> {
  Barcode? result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  List<String> scannedCodeList = <String>[];

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  @override
  void initState() {
    scannedCodeList.clear();
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(child: _buildQrView(context)),
        ],
      ),
    );
  }

  getScannedCode(String strings) {
    if(controller != null) {
      controller!.pauseCamera();
      scannedCodeList.add(strings);
      print('ScannedData: ${scannedCodeList.length}');
      // return scannedCodeList;
    }
  }

  Widget _buildQrView(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 || MediaQuery.of(context).size.height < 400) ? 240.0 : 340.0;
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(borderColor: Colors.red, borderRadius: 10, borderLength: 30, borderWidth: 10, cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('no Permission')),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      // getScannedCode(scanData.code);
      controller.pauseCamera();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Scanned Code'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Data: ${scanData.code}'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Scan More'),
                onPressed: () {
                  Navigator.of(context).pop();
                  getScannedCode(scanData.code);
                  controller.resumeCamera();
                },
              ),
              TextButton(
                child: Text('Ok'),
                onPressed: () {
                  getScannedCode(scanData.code);
                  Navigator.of(context).pop(scannedCodeList);
                  Navigator.of(context).pop(scannedCodeList);
                },
              ),
            ],
          );
        },
      );
    });
  }
}
