import 'dart:io';

import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Redeem extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RedeemState();
  }
  
}
class RedeemState extends State<Redeem>{
  var loading = true;
    @override
  void initState() {
      // Enable hybrid composition.
      if (Platform.isAndroid) {
        WebView.platform = SurfaceAndroidWebView();
      }
      super.initState();
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        body:Stack(
          children: [
            WebView(

              initialUrl: ConstantString.redeem_url,
              javascriptMode: JavascriptMode.unrestricted,

              onPageStarted: (url) {
                loading = true;
                setState(() {

                });
              },
              onProgress: (int progress) {
                print("WebView is loading (progress : $progress%)");
              },
              onPageFinished: (String url) {
                print('Page finished loading: $url');

                Future.delayed(const Duration(seconds: 0),(){
                  loading = false;
                  setState(() {

                  });
                });
              },
            ),
            if(loading)Container(constraints: const BoxConstraints.expand(),color:Colors.white, child: const Center(child: CircularProgressIndicator()))
          ],
        ),

      );
    }
  }
