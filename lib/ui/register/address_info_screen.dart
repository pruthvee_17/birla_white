import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/register/bank_info_screen.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:intl/intl.dart';

class AddressInfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddressInfoScreenState();
  }
}

class AddressInfoScreenState extends State<AddressInfoScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController addressLine1Controller = TextEditingController();
  final TextEditingController addressLine2Controller = TextEditingController();
  final TextEditingController pincodeController = TextEditingController();
  final TextEditingController motherNameController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController stateController = TextEditingController();
  final TextEditingController dobDate = TextEditingController();
  DateTime selectedDobDate = DateTime.now();
  final TextEditingController domDate = TextEditingController();
  DateTime selectedDomDate = DateTime.now();

  final FocusNode addressLine1Focus = FocusNode();
  final FocusNode addressLine2Focus = FocusNode();
  final FocusNode pincodeFocus = FocusNode();
  final FocusNode motherNameFocus = FocusNode();
  final FocusNode cityFocus = FocusNode();
  final FocusNode stateFocus = FocusNode();
  final FocusNode dobFocus = FocusNode();
  final FocusNode domFocus = FocusNode();

  bool isError = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    addressLine1Focus.addListener(() {
      setState(() {});
    });
    addressLine2Focus.addListener(() {
      setState(() {});
    });
    pincodeFocus.addListener(() {
      setState(() {});
    });
    motherNameFocus.addListener(() {
      setState(() {});
    });
    cityFocus.addListener(() {
      setState(() {});
    });
    stateFocus.addListener(() {
      setState(() {});
    });
    dobFocus.addListener(() {
      setState(() {});
    });
    domFocus.addListener(() {
      setState(() {});
    });

    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.signup, context),
      body: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: ColorConstant.appDarkColor.withOpacity(0.1),
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black.withOpacity(0.1),
                    width: 2.0,
                  ),
                ),
              ),
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 15, top: 8),
                    child: CircularPercentIndicator(
                      radius: 75.0,
                      lineWidth: 5.0,
                      percent: 0.6,
                      center: Text(
                        "3/5",
                        style: Utility.subtitleBlack_18(context),
                      ),
                      progressColor: ColorConstant.appDarkColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, top: 25.0, bottom: 15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          ConstantString.address_information,
                          textAlign: TextAlign.right,
                          style: Utility.subtitleBlack_18(context),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 90.0),
                          child: Text(
                            'Next : ' + ConstantString.bank_details,
                            textAlign: TextAlign.right,
                            style: Utility.subtitleBoldGrey_11(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              child: SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: addressLine1Controller,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: addressLine1Focus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_address_line_1;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_address_line_1, ConstantString.address_line_1, addressLine1Focus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: addressLine2Controller,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: addressLine2Focus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_address_line_2;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_address_line_2, ConstantString.address_line_2, addressLine2Focus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: cityController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: cityFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_city;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(context, ConstantString.enter_city, ConstantString.city, cityFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: stateController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: stateFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_state;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration:
                                Utility.editBoxInputDecoration(context, ConstantString.enter_state, ConstantString.state, stateFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: pincodeController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: pincodeFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_pincode;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            ],
                            textInputAction: TextInputAction.next,
                            maxLength: 10,
                            style: Utility.subtitleBlack_14(context),
                            decoration:
                                Utility.editBoxInputDecoration(context, ConstantString.enter_pincode, ConstantString.pincode, pincodeFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: GestureDetector(
                            onTap: () => _selectDobDate(context),
                            child: AbsorbPointer(
                              child: TextFormField(
                                controller: dobDate,
                                focusNode: dobFocus,
                                onChanged: (value) {
                                  if (isError) {
                                    _formKey?.currentState!.validate();
                                  }
                                },
                                validator: (val) {
                                  if (val == null || val.isEmpty) {
                                    isError = true;
                                    return ConstantString.select_dob;
                                  }
                                  isError = false;
                                },
                                keyboardType: TextInputType.datetime,
                                decoration: Utility.editBoxInputDecoration(context, ConstantString.select_dob, ConstantString.dob, dobFocus.hasFocus),
                              ),
                            ),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: GestureDetector(
                            onTap: () => _selectDomDate(context),
                            child: AbsorbPointer(
                              child: TextFormField(
                                controller: domDate,
                                focusNode: domFocus,
                                onChanged: (value) {
                                  if (isError) {
                                    _formKey?.currentState!.validate();
                                  }
                                },
                                validator: (val) {
                                  if (val == null || val.isEmpty) {
                                    isError = true;
                                    return ConstantString.select_dom;
                                  }
                                  isError = false;
                                },
                                keyboardType: TextInputType.datetime,
                                decoration: Utility.editBoxInputDecoration(context, ConstantString.select_dom, ConstantString.dom, domFocus.hasFocus),
                              ),
                            ),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: motherNameController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: motherNameFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_mother_name;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_mother_name, ConstantString.mother_name, motherNameFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0, right: 20.0, bottom: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  child: Text(ConstantString.back, style: Utility.buttonTextStyle_14(context)),
                                  style: Utility.buttonStyle(context),
                                  onPressed: () {
                                    if (!_formKey!.currentState!.validate()) {
                                      Navigator.of(context).pop();
                                    }
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0, right: 10.0, bottom: 10),
                                child: Align(
                                  alignment: Alignment.bottomRight,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.save_next, style: Utility.buttonTextStyle_14(context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                     // if (_formKey!.currentState!.validate()) {
                                        FocusScope.of(context).unfocus();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => BankInfoScreen()),
                                        );
                                    //  }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<Null> _selectDobDate(BuildContext context) async {
    final DateTime? picked =
        await showDatePicker(context: context, initialDate: selectedDobDate, firstDate: DateTime(1901, 1), lastDate: DateTime.now());
    if (picked != null && picked != selectedDobDate) {
      setState(() {
        final DateFormat formatter = DateFormat('dd-MM-yyyy');
        final String formatted = formatter.format(picked);

        selectedDobDate = picked;
        dobDate.value = TextEditingValue(text: formatted.toString());
      });
    }
  }

  Future<Null> _selectDomDate(BuildContext context) async {
    final DateTime? picked =
        await showDatePicker(context: context, initialDate: selectedDomDate, firstDate: DateTime(1901, 1), lastDate: DateTime.now());
    if (picked != null && picked != selectedDomDate) {
      setState(() {
        final DateFormat formatter = DateFormat('dd-MM-yyyy');
        final String formatted = formatter.format(picked);
        selectedDomDate = picked;
        domDate.value = TextEditingValue(text: formatted.toString());
      });
    }
  }
}
