import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import 'misc_info_screen.dart';

class BankInfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BankInfoScreenState();
  }
}

class BankInfoScreenState extends State<BankInfoScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController bankNoController = TextEditingController();
  final TextEditingController bankNameController = TextEditingController();
  final TextEditingController ifscCodeController = TextEditingController();
  final TextEditingController upiController = TextEditingController();
  final TextEditingController identifyController = TextEditingController();
  final TextEditingController addressController = TextEditingController();

  final FocusNode bankNoFocus = FocusNode();
  final FocusNode bankNameFocus = FocusNode();
  final FocusNode ifscCodeFocus = FocusNode();
  final FocusNode upiFocus = FocusNode();
  final FocusNode identifyFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();
  bool isError = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    bankNoFocus.addListener(() {
      setState(() {});
    });
    bankNameFocus.addListener(() {
      setState(() {});
    });
    ifscCodeFocus.addListener(() {
      setState(() {});
    });
    upiFocus.addListener(() {
      setState(() {});
    });
    identifyFocus.addListener(() {
      setState(() {});
    });
    addressFocus.addListener(() {
      setState(() {});
    });


    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.signup, context),
      body: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: ColorConstant.appDarkColor.withOpacity(0.1),
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black.withOpacity(0.1),
                    width: 2.0,
                  ),
                ),
              ),
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 15, top: 8),
                    child: CircularPercentIndicator(
                      radius: 75.0,
                      lineWidth: 5.0,
                      percent: 0.8,
                      center: Text(
                        "4/5",
                        style: Utility.subtitleBlack_18(context),
                      ),
                      progressColor: ColorConstant.appDarkColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, top: 25.0, bottom: 15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          ConstantString.bank_details,
                          textAlign: TextAlign.right,
                          style: Utility.subtitleBlack_18(context),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 90.0),
                          child: Text(
                            'Next : ' + ConstantString.misc_details,
                            textAlign: TextAlign.right,
                            style: Utility.subtitleBoldGrey_11(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              child: SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Utility.uiSizeBox(0.0, 20.0),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                              ConstantString.bank_details,
                              style: Utility.subtitleBoldBlack_16(context)
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: bankNoController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: bankNoFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_bank_ac_no;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_bank_ac_no, ConstantString.bank_ac_no, bankNoFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: bankNameController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: bankNameFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_bank_ac_name;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_bank_ac_name, ConstantString.bank_ac_name, bankNameFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: ifscCodeController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: ifscCodeFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_ifsc_code;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_ifsc_code, ConstantString.ifsc_code, ifscCodeFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 20.0, bottom: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: ElevatedButton(
                              child: Text(ConstantString.submit, style: Utility.buttonTextStyle_14(context)),
                              style: Utility.buttonStyle(context),
                              onPressed: () {

                              },
                            ),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                              ConstantString.upi_details,
                              style: Utility.subtitleBoldBlack_16(context)
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: upiController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: upiFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_upi_details;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_upi_details, ConstantString.upi_details, upiFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 20.0, bottom: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: ElevatedButton(
                              child: Text(ConstantString.submit, style: Utility.buttonTextStyle_14(context)),
                              style: Utility.buttonStyle(context),
                              onPressed: () {

                              },
                            ),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                              ConstantString.kyc_details,
                              style: Utility.subtitleBoldBlack_16(context)
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: identifyController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: identifyFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_identity_proof;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_identity_proof, ConstantString.identity_proof, identifyFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: addressController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: addressFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_address_proof;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_address_proof, ConstantString.address_proof, addressFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 20.0, bottom: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: ElevatedButton(
                              child: Text(ConstantString.submit, style: Utility.buttonTextStyle_14(context)),
                              style: Utility.buttonStyle(context),
                              onPressed: () {

                              },
                            ),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0, right: 20.0, bottom: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  child: Text(ConstantString.back, style: Utility.buttonTextStyle_14(context)),
                                  style: Utility.buttonStyle(context),
                                  onPressed: () {
                                    if (!_formKey!.currentState!.validate()) {
                                      Navigator.of(context).pop();
                                    }
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0, right: 10.0, bottom: 10),
                                child: Align(
                                  alignment: Alignment.bottomRight,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.save_next, style: Utility.buttonTextStyle_14(context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                      //if (_formKey!.currentState!.validate()) {
                                        FocusScope.of(context).unfocus();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => MiscInfoScreen()),
                                        );
                                     // }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
