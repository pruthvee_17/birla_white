import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/home/home_screen_widget.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class MiscInfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MiscInfoScreenState();
  }
}

class MiscInfoScreenState extends State<MiscInfoScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController preferredRetailerController = TextEditingController();

  final FocusNode preferredRetailerFocus = FocusNode();

  bool isError = false;
  bool term_condition = false;
  bool privacy_policy = false;
  var selectedWork;
  var businessWorkList = ['Individual', 'Government', 'Contractor'];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.signup, context),
      body: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: ColorConstant.appDarkColor.withOpacity(0.1),
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black.withOpacity(0.1),
                    width: 2.0,
                  ),
                ),
              ),
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 15, top: 8),
                    child: CircularPercentIndicator(
                      radius: 75.0,
                      lineWidth: 5.0,
                      percent: 1.0,
                      center: Text(
                        "5/5",
                        style: Utility.subtitleBlack_18(context),
                      ),
                      progressColor: ColorConstant.appDarkColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, top: 25.0, bottom: 15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      // / crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          ConstantString.misc_details,
                          textAlign: TextAlign.right,
                          style: Utility.subtitleBlack_18(context),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              child: SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Utility.uiSizeBox(0.0, 10.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            autofocus: false,
                            showCursor: true,
                            controller: preferredRetailerController,
                            onChanged: (value) {
                              if (isError) {
                                _formKey?.currentState!.validate();
                              }
                            },
                            focusNode: preferredRetailerFocus,
                            validator: (val) {
                              if (val == null || val.isEmpty) {
                                isError = true;
                                return ConstantString.enter_preferred_retailer;
                              } else if (val.length != 10) {
                                isError = true;
                                return ConstantString.enter_valid_preferred_retailer;
                              }
                              isError = false;
                            },
                            keyboardType: TextInputType.number,
                            maxLength: 10,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            ],
                            textInputAction: TextInputAction.next,
                            style: Utility.subtitleBlack_14(context),
                            decoration: Utility.editBoxInputDecoration(
                                context, ConstantString.enter_preferred_retailer, ConstantString.preferred_retailer, preferredRetailerFocus.hasFocus),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 10.0),
                        Container(
                          padding: const EdgeInsets.only(left: 5.0, right: 8.0),
                          child: DropdownButtonFormField<String>(
                            value: selectedWork,
                            hint: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Text(ConstantString.business_work, style: Utility.subtitleDarkGrey_14(context)),
                            ),
                            onChanged: (salutation) {
                              setState(() {
                                selectedWork = salutation;
                              });
                            },
                            validator: (value) => value == null ? ConstantString.select_business_work : null,
                            items: businessWorkList.map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Text(value, style: Utility.subtitleBlack_14(context)),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 30.0),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Stack(
                            alignment: Alignment.centerLeft,
                            children: [
                              Checkbox(
                                value: term_condition,
                                onChanged: (bool? value) {
                                  setState(() {
                                    term_condition = value!;
                                  });
                                },
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 38),
                                child: Text(
                                  ConstantString.term_condition,
                                  style: Utility.subtitleAppColor_14(context),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Stack(
                            alignment: Alignment.centerLeft,
                            children: [
                              Checkbox(
                                value: privacy_policy,
                                onChanged: (bool? value) {
                                  setState(() {
                                    privacy_policy = value!;
                                  });
                                },
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 38),
                                child: Text(
                                  ConstantString.privacy_policy,
                                  style: Utility.subtitleAppColor_14(context),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Utility.uiSizeBox(0.0, 20.0),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 14.0, right: 20.0, bottom: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(
                                  child: Text(ConstantString.back, style: Utility.buttonTextStyle_14(context)),
                                  style: Utility.buttonStyle(context),
                                  onPressed: () {
                                    if (!_formKey!.currentState!.validate()) {
                                      Navigator.of(context).pop();
                                    }
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0, right: 10.0, bottom: 10),
                                child: Align(
                                  alignment: Alignment.bottomRight,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.submit, style: Utility.buttonTextStyle_14(context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                      // if (_formKey!.currentState!.validate()) {
                                      FocusScope.of(context).unfocus();
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                            builder: (context) => HomeScreenWidget(),
                                          ),
                                              (route) => false);
                                      //}
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
