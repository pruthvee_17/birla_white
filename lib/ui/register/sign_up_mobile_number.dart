import 'dart:async';

import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/home/home_screen_widget.dart';
import 'package:birla_white/ui/register/profile_info_screen.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import 'bank_info_screen.dart';

class SignUpMobileNumber extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignUpMobileNumberState();
  }
}

class SignUpMobileNumberState extends State<SignUpMobileNumber> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController mobileNoController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  final FocusNode _mobileNoFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  bool passwordVisible = false;
  bool rememberMe = false;
  bool isError = false;
  bool _isOtpVisible = false;
  bool _isMobileVisible = true;
  bool _isGetOTPBtnVisible = true;
  bool _isVerifyBtnVisible = false;
  bool _isResendEnable = false;
  String otpWaitTimeLabel = "";
  late Timer _timer;
  int _start = 60;
  bool isEnable = false;
  int count = 1;
  final TextEditingController otp1Controller = TextEditingController();
  final TextEditingController otp2Controller = TextEditingController();
  final TextEditingController otp3Controller = TextEditingController();
  final TextEditingController otp4Controller = TextEditingController();
  final TextEditingController otp5Controller = TextEditingController();
  final TextEditingController otp6Controller = TextEditingController();
  final FocusNode otp1Focus = FocusNode();
  final FocusNode otp2Focus = FocusNode();
  final FocusNode otp3Focus = FocusNode();
  final FocusNode otp4Focus = FocusNode();
  final FocusNode otp5Focus = FocusNode();
  final FocusNode otp6Focus = FocusNode();

  @override
  void initState() {
    otp1Focus.addListener(() {
      otp1Controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: otp1Controller.text.length,
      );
    });
    otp2Focus.addListener(() {
      otp2Controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: otp2Controller.text.length,
      );
    });
    otp3Focus.addListener(() {
      otp3Controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: otp3Controller.text.length,
      );
    });
    otp4Focus.addListener(() {
      otp4Controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: otp4Controller.text.length,
      );
    });
    otp5Focus.addListener(() {
      otp5Controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: otp5Controller.text.length,
      );
    });
    otp6Focus.addListener(() {
      otp6Controller.selection = TextSelection(
        baseOffset: 0,
        extentOffset: otp6Controller.text.length,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _mobileNoFocus.addListener(() {
      setState(() {});
    });
    _passwordFocus.addListener(() {
      setState(() {});
    });

    return Scaffold(
      backgroundColor: ColorConstant.white,
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: Container(
        color: ColorConstant.white,
        height: 144,
        child: Stack(
          children: [
            //-----------------Verify---------------//
            Visibility(
              visible: _isVerifyBtnVisible,
              child: Column(
                children: [
                  Utility.uiSizeBox(0.0, 5.0),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                      right: 10.0,
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: ElevatedButton(
                        child: Text(ConstantString.verify, style: Utility.buttonTextStyle_14(context)),
                        style: Utility.buttonStyle(context),
                        onPressed: () async {
                          if (_formKey!.currentState!.validate()) {
                            _isOtpVisible = false;
                            _isMobileVisible = true;
                            _isGetOTPBtnVisible = true;
                            _isVerifyBtnVisible = false;
                            FocusScope.of(context).unfocus();
                            Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ProfileInfoScreen()));
                          } else {
                            print("Error");
                          }
                        },
                      ),
                    ),
                  ),
                  Utility.uiSizeBox(0.0, 15.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        ConstantString.dont_receive_the_otp,
                        style: Utility.subtitleGrey_14(context),
                      ),
                      InkWell(
                        onTap: () async {
                          if (isEnable) {
                            setState(() {
                              isEnable = false;
                            });
                            startTimer();
                            //signInWithPhone(widget.phone);
                          }
                        },
                        child: Text(
                          ConstantString.resend_otp,
                          style: isEnable ? Utility.subtitleAppColor_14(context) : Utility.subtitleGrey_14(context),
                        ),
                      ),
                    ],
                  ),
                  Utility.uiSizeBox(0.0, 20.0),
                  //  Text(ConstantString.sale_team_login, textAlign: TextAlign.center, style: Utility.subtitleUnderlineAppDarkColor_14(context)),
                  //Utility.uiSizeBox(0.0, 5.0),
                ],
              ),
            ),

            //-----------------GET OTP---------------//
            Visibility(
              visible: _isGetOTPBtnVisible,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 10.0, bottom: 10),
                child: Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    child: Text(ConstantString.get_otp, style: Utility.buttonTextStyle_14(context)),
                    style: Utility.buttonStyle(context),
                    onPressed: () {
                      if (_formKey!.currentState!.validate()) {
                        FocusScope.of(context).unfocus();
                        _isOtpVisible = true;
                        _isMobileVisible = false;
                        _isGetOTPBtnVisible = false;
                        _isVerifyBtnVisible = true;
                        otp1Controller.clear();
                        otp2Controller.clear();
                        otp3Controller.clear();
                        otp4Controller.clear();
                        otp5Controller.clear();
                        otp6Controller.clear();
                        startTimer();
                        setState(() {});
                      } else {
                        print("Error");
                      }
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      appBar: Utility.commonAppBar(ConstantString.signup, context),
      body: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: ColorConstant.appDarkColor.withOpacity(0.1),
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black.withOpacity(0.1),
                    width: 2.0,
                  ),
                ),
              ),
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 15, top: 8),
                    child: CircularPercentIndicator(
                      radius: 75.0,
                      lineWidth: 5.0,
                      percent: 0.2,
                      center: Text(
                        "1/5",
                        style: Utility.subtitleBlack_18(context),
                      ),
                      progressColor: ColorConstant.appDarkColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, top: 25.0, bottom: 15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          ConstantString.mobile_verification,
                          textAlign: TextAlign.right,
                          style: Utility.subtitleBlack_18(context),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 90.0),
                          child: Text(
                            'Next : ' + ConstantString.profile_information,
                            textAlign: TextAlign.right,
                            style: Utility.subtitleBoldGrey_11(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              child: Container(
                color: ColorConstant.white,
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, top: 50, bottom: 50),
                        child: Column(
                          children: [
                            Visibility(
                              visible: _isMobileVisible,
                              child: Column(
                                children: [
                                  Text(
                                    ConstantString.we_will_send_you_one_time_code_on_your_phone_number,
                                    textAlign: TextAlign.center,
                                    style: Utility.headlineGrey_14(context),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 22.0),
                                    child: TextFormField(
                                      autofocus: false,
                                      showCursor: true,
                                      controller: mobileNoController,
                                      onChanged: (value) {
                                        if (isError) {
                                          _formKey?.currentState!.validate();
                                        }
                                      },
                                      focusNode: _mobileNoFocus,
                                      validator: (val) {
                                        if (val == null || val.isEmpty) {
                                          isError = true;
                                          return ConstantString.enter_mobile_no;
                                        } else if (val.length != 10) {
                                          isError = true;
                                          return ConstantString.enter_valid_mobile_no;
                                        }
                                        isError = false;
                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: <TextInputFormatter>[
                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                      ],
                                      textInputAction: TextInputAction.done,
                                      style: Utility.subtitleBlack_14(context),
                                      maxLength: 10,
                                      decoration: Utility.editBoxInputDecoration(context, ConstantString.enter_mobile_no, ConstantString.mobile_no, _mobileNoFocus.hasFocus),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 15.0),
                            Visibility(
                                visible: _isOtpVisible,
                                child: Column(
                                  children: [
                                    Stack(
                                      children: [
                                        RichText(
                                          text: TextSpan(
                                            text: ConstantString.enter_the_6_digit_code_sent_to_you_at,
                                            style: Utility.headlineGrey_14(context),
                                            children: <TextSpan>[
                                              TextSpan(text: "( " + mobileNoController.text + " )", style: Utility.headlineBlack_17(context)),
                                            ],
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(left: 180, top: 5),
                                          child: IconButton(
                                            onPressed: () {
                                              _isOtpVisible = false;
                                              _isMobileVisible = true;
                                              _isGetOTPBtnVisible = true;
                                              _isVerifyBtnVisible = false;
                                              _timer.cancel();
                                              _start = 60;
                                              setState(() {});
                                              //   Navigator.of(context).pop();
                                            },
                                            icon: Icon(
                                              Icons.edit,
                                              size: 17,
                                              color: ColorConstant.black,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),

                                    //------------------------------OTP-----------------------------//
                                    Center(
                                      child: Container(
                                          margin: const EdgeInsets.only(top: 20.0, bottom: 10.0, left: 10.0, right: 10.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: [
                                              SizedBox(
                                                // height: 50.0,
                                                width: 40.0,
                                                child: Container(
                                                  child: Center(
                                                    child: TextFormField(
                                                      controller: otp1Controller,
                                                      keyboardType: TextInputType.number,
                                                      focusNode: otp1Focus,
                                                      showCursor: true,
                                                      textInputAction: TextInputAction.next,
                                                      textAlign: TextAlign.center,
                                                      cursorColor: Theme.of(context).primaryColor,
                                                      decoration: InputDecoration(
                                                          border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.appDarkColor),
                                                          ),
                                                          enabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          disabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          contentPadding: EdgeInsets.zero),
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(1),
                                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                      ],
                                                      style: Utility.subtitleBlack_14(context),

                                                      onTap: () {
                                                        setState(() {
                                                          otp1Focus.context;
                                                        });
                                                        otp1Controller.selection = TextSelection(
                                                          baseOffset: 0,
                                                          extentOffset: otp1Controller.text.length,
                                                        );
                                                      },
                                                      onChanged: (text) {
                                                        if (text.length == 1) {
                                                          setState(() {
                                                            _fieldFocusChange(context, otp1Focus, otp2Focus);
                                                          });
                                                        }
                                                      },
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          /* isError = true;
                                                          Utility.showToast(
                                                              "Please enter valid otp !");*/
                                                        }
                                                        return null;
                                                      },
                                                      // maxLength: 1,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 40.0,
                                                child: Container(
                                                  child: Center(
                                                    child: TextFormField(
                                                      controller: otp2Controller,
                                                      keyboardType: TextInputType.number,
                                                      focusNode: otp2Focus,
                                                      textInputAction: TextInputAction.next,
                                                      textAlign: TextAlign.center,
                                                      cursorColor: Theme.of(context).primaryColor,
                                                      decoration: InputDecoration(
                                                          border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                                          ),
                                                          enabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          disabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          contentPadding: EdgeInsets.zero),
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(1),
                                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                      ],
                                                      style: Utility.subtitleBlack_14(context),
                                                      onTap: () {
                                                        setState(() {
                                                          otp2Focus.context;
                                                        });
                                                        otp2Controller.selection = TextSelection(
                                                          baseOffset: 0,
                                                          extentOffset: otp2Controller.text.length,
                                                        );
                                                      },
                                                      onChanged: (text) {
                                                        if (text.length == 1) {
                                                          setState(() {
                                                            _fieldFocusChange(context, otp2Focus, otp3Focus);
                                                          });
                                                        }
                                                        if (text.isEmpty) {
                                                          FocusScope.of(context).requestFocus(otp1Focus);
                                                        }
                                                      },
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          /* Utility.showToast(
                                                              "Please enter valid otp !");*/
                                                        }
                                                        return null;
                                                      },
                                                      // maxLength: 1,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 40.0,
                                                child: Container(
                                                  child: Center(
                                                    child: TextFormField(
                                                      controller: otp3Controller,
                                                      keyboardType: TextInputType.number,
                                                      focusNode: otp3Focus,
                                                      textInputAction: TextInputAction.next,
                                                      textAlign: TextAlign.center,
                                                      cursorColor: Theme.of(context).primaryColor,
                                                      decoration: InputDecoration(
                                                          border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                                          ),
                                                          enabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          disabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          contentPadding: EdgeInsets.zero),
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(1),
                                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                      ],
                                                      style: Utility.subtitleBlack_14(context),
                                                      onTap: () {
                                                        setState(() {
                                                          otp3Focus.context;
                                                        });
                                                        otp3Controller.selection = TextSelection(
                                                          baseOffset: 0,
                                                          extentOffset: otp3Controller.text.length,
                                                        );
                                                      },
                                                      onChanged: (text) {
                                                        if (text.length == 1) {
                                                          setState(() {
                                                            _fieldFocusChange(context, otp3Focus, otp4Focus);
                                                          });
                                                        }
                                                        if (text.isEmpty) {
                                                          FocusScope.of(context).requestFocus(otp2Focus);
                                                        }
                                                      },
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          /* Utility.showToast(
                                                              "Please enter valid otp !");*/
                                                        }
                                                        return null;
                                                      },
                                                      // maxLength: 1,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 40.0,
                                                child: Container(
                                                  child: Center(
                                                    child: TextFormField(
                                                      controller: otp4Controller,
                                                      keyboardType: TextInputType.number,
                                                      focusNode: otp4Focus,
                                                      textInputAction: TextInputAction.next,
                                                      textAlign: TextAlign.center,
                                                      cursorColor: Theme.of(context).primaryColor,
                                                      decoration: InputDecoration(
                                                          border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                                          ),
                                                          enabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          disabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          contentPadding: EdgeInsets.zero),
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(1),
                                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                      ],
                                                      style: Utility.subtitleBlack_14(context),
                                                      onTap: () {
                                                        setState(() {
                                                          otp4Focus.context;
                                                        });
                                                        otp4Controller.selection = TextSelection(
                                                          baseOffset: 0,
                                                          extentOffset: otp4Controller.text.length,
                                                        );
                                                      },
                                                      onChanged: (text) {
                                                        if (text.length == 1) {
                                                          setState(() {
                                                            _fieldFocusChange(context, otp4Focus, otp5Focus);
                                                          });
                                                        }
                                                        if (text.isEmpty) {
                                                          FocusScope.of(context).requestFocus(otp3Focus);
                                                        }
                                                      },
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          /* Utility.showToast(
                                                              "Please enter valid otp !");*/
                                                        }
                                                        return null;
                                                      },
                                                      // maxLength: 1,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 40.0,
                                                child: Container(
                                                  child: Center(
                                                    child: TextFormField(
                                                      controller: otp5Controller,
                                                      keyboardType: TextInputType.number,
                                                      focusNode: otp5Focus,
                                                      textInputAction: TextInputAction.next,
                                                      textAlign: TextAlign.center,
                                                      cursorColor: Theme.of(context).primaryColor,
                                                      decoration: InputDecoration(
                                                          border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                                          ),
                                                          enabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          disabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          contentPadding: EdgeInsets.zero),
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(1),
                                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                      ],
                                                      style: Utility.subtitleBlack_14(context),
                                                      onTap: () {
                                                        setState(() {
                                                          otp5Focus.context;
                                                        });
                                                        otp5Controller.selection = TextSelection(
                                                          baseOffset: 0,
                                                          extentOffset: otp5Controller.text.length,
                                                        );
                                                      },
                                                      onChanged: (text) {
                                                        if (text.length == 1) {
                                                          setState(() {
                                                            _fieldFocusChange(context, otp5Focus, otp6Focus);
                                                          });
                                                        }
                                                        if (text.isEmpty) {
                                                          FocusScope.of(context).requestFocus(otp4Focus);
                                                        }
                                                      },
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          /* Utility.showToast(
                                                              "Please enter valid otp !");*/
                                                        }
                                                        return null;
                                                      },
                                                      // maxLength: 1,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 40.0,
                                                child: Container(
                                                  child: Center(
                                                    child: TextFormField(
                                                      controller: otp6Controller,
                                                      keyboardType: TextInputType.number,
                                                      focusNode: otp6Focus,
                                                      textInputAction: TextInputAction.done,
                                                      textAlign: TextAlign.center,
                                                      cursorColor: Theme.of(context).primaryColor,
                                                      decoration: InputDecoration(
                                                          border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          focusedBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: Theme.of(context).primaryColor),
                                                          ),
                                                          enabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            borderSide: BorderSide(color: ColorConstant.darkGrey),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          disabledBorder: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                          ),
                                                          contentPadding: EdgeInsets.zero),
                                                      inputFormatters: [
                                                        LengthLimitingTextInputFormatter(1),
                                                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                      ],
                                                      style: Utility.subtitleBlack_14(context),
                                                      onTap: () {
                                                        setState(() {
                                                          otp6Focus.context;
                                                        });
                                                        otp6Controller.selection = TextSelection(
                                                          baseOffset: 0,
                                                          extentOffset: otp6Controller.text.length,
                                                        );
                                                      },
                                                      onChanged: (text) {
                                                        if (text.length == 1) {
                                                          FocusScope.of(context).requestFocus(FocusNode());
                                                        }
                                                        if (text.isEmpty) {
                                                          FocusScope.of(context).requestFocus(otp5Focus);
                                                        }
                                                      },
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          /*  Utility.showToast(
                                                              "Please enter valid otp !");*/
                                                        }
                                                        return null;
                                                      },
                                                      // maxLength: 1,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )),
                                    ),

                                    Utility.uiSizeBox(0.0, 15.0),
                                    !isEnable
                                        ? Text(
                                            ' Resend otp in  $_start seconds',
                                            style: Utility.subtitleGrey_14(context),
                                          )
                                        : Container(),
                                  ],
                                )),
                            Utility.uiSizeBox(0.0, 15.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void startTimer() async {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            isEnable = true;
            timer.cancel();
            _start = 60;
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
    // Future.delayed(Duration(seconds: 5)).then((value) => readingOtp());
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
