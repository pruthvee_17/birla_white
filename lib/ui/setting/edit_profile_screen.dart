import 'dart:io';

import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class EditProfileInfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return EditProfileInfoScreenState();
  }
}

class EditProfileInfoScreenState extends State<EditProfileInfoScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController mobileNumberController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController salutationController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController firmNameController = TextEditingController();
  final TextEditingController contractorNameController =
      TextEditingController();
  final TextEditingController contractorMobileNoController =
      TextEditingController();
  final TextEditingController noOfPainterController = TextEditingController();
  final TextEditingController nameController = TextEditingController();

  final FocusNode mobileNumberFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();
  final FocusNode salutationFocus = FocusNode();
  final FocusNode firstNameFocus = FocusNode();
  final FocusNode lastNameFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();
  final FocusNode painterTypeFocus = FocusNode();
  final FocusNode applicatorCategoryFocus = FocusNode();
  final FocusNode firmNameFocus = FocusNode();
  final FocusNode contractorNameFocus = FocusNode();
  final FocusNode contractorMobileNoFocus = FocusNode();
  final FocusNode noOfPainterFocus = FocusNode();

  XFile? _image;
  String imageUrl = "";
  double iconSize = 18;
  bool isError = false;
  bool isPainter = false;
  bool isContractor = false;
  bool passwordVisible = false;
  var selectedSalutation;
  var salutationItems = ['Mr.', 'Mrs.'];

  var selectedApplicator;
  var applicatorCategory = ['Painter', 'Contractor'];

  var selectedPainterType;
  var painterType = ['Individual', 'On Contract'];

  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    mobileNumberFocus.addListener(() {
      setState(() {});
    });
    salutationFocus.addListener(() {
      setState(() {});
    });
    firstNameFocus.addListener(() {
      setState(() {});
    });
    lastNameFocus.addListener(() {
      setState(() {});
    });
    emailFocus.addListener(() {
      setState(() {});
    });
    firmNameFocus.addListener(() {
      setState(() {});
    });
    contractorNameFocus.addListener(() {
      setState(() {});
    });
    contractorMobileNoFocus.addListener(() {
      setState(() {});
    });
    noOfPainterFocus.addListener(() {
      setState(() {});
    });
    salutationFocus.addListener(() {
      setState(() {});
    });
    painterTypeFocus.addListener(() {
      setState(() {});
    });
    applicatorCategoryFocus.addListener(() {
      setState(() {});
    });

    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.edit_profile, context),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
      body: Column(
        children: [
          Flexible(
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(
                    left: 10, right: 10, top: 10, bottom: 10),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: mobileNumberController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: mobileNumberFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_mobile_no;
                            } else if (val.length != 10) {
                              isError = true;
                              return ConstantString.enter_valid_mobile_no;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          ],
                          textInputAction: TextInputAction.next,
                          maxLength: 10,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context,
                              ConstantString.enter_mobile_no,
                              ConstantString.mobile_no,
                              mobileNumberFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Container(
                        padding: const EdgeInsets.only(left: 5.0, right: 8.0),
                        child: DropdownButtonFormField<String>(
                          value: selectedSalutation,
                          focusNode: salutationFocus,
                          hint: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Text(ConstantString.salutation,
                                style: Utility.subtitleDarkGrey_14(context)),
                          ),
                          onChanged: (salutation) {
                            setState(() {
                              isError = false;
                              selectedSalutation = salutation;
                            });
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              isError = true;
                              return ConstantString.select_salutation;
                            }
                            isError = false;
                            // return value == null ? ConstantString.select_salutation : null;
                          },
                          items: salutationItems
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(value,
                                    style: Utility.subtitleBlack_14(context)),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: firstNameController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: firstNameFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_first_name;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context,
                              ConstantString.enter_first_name,
                              ConstantString.first_name,
                              firstNameFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: lastNameController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: lastNameFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_last_name;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context,
                              ConstantString.enter_last_name,
                              ConstantString.last_name,
                              lastNameFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: emailController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: emailFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_email;
                            } else if (!Utility.validEmail(val)) {
                              isError = true;
                              return ConstantString.enter_valid_email;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context,
                              ConstantString.enter_email,
                              ConstantString.email + "*",
                              emailFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Container(
                        padding: const EdgeInsets.only(left: 5.0, right: 8.0),
                        child: DropdownButtonFormField<String>(
                          value: selectedApplicator,
                          focusNode: applicatorCategoryFocus,
                          hint: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Text(ConstantString.applicator_category,
                                style: Utility.subtitleDarkGrey_14(context)),
                          ),
                          onChanged: (salutation) {
                            setState(() {
                              if (salutation == ("Painter")) {
                                isContractor = false;
                                isPainter = true;
                                selectedApplicator = salutation;
                              } else if (salutation == ("Contractor")) {
                                isContractor = true;
                                isPainter = false;
                                selectedApplicator = salutation;
                              }
                            });
                          },
                          validator: (value) => value == null
                              ? ConstantString.select_applicator_category
                              : null,
                          items: applicatorCategory
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(value,
                                    style: Utility.subtitleBlack_14(context)),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Visibility(
                        visible: isPainter,
                        child: Column(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.only(left: 5.0, right: 8.0),
                              child: DropdownButtonFormField<String>(
                                value: selectedPainterType,
                                focusNode: painterTypeFocus,
                                hint: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Text(ConstantString.painter_type,
                                      style:
                                          Utility.subtitleDarkGrey_14(context)),
                                ),
                                onChanged: (salutation) => setState(
                                    () => selectedPainterType = salutation),
                                validator: (value) => value == null
                                    ? ConstantString.select_painter_type
                                    : null,
                                items: painterType
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Text(value,
                                          style: Utility.subtitleBlack_14(
                                              context)),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 10.0),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: isContractor,
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: TextFormField(
                                autofocus: false,
                                showCursor: true,
                                controller: contractorNameController,
                                onChanged: (value) {
                                  if (isError) {
                                    _formKey?.currentState!.validate();
                                  }
                                },
                                focusNode: contractorNameFocus,
                                validator: (val) {
                                  if (val == null || val.isEmpty) {
                                    isError = true;
                                    return ConstantString.enter_contractor_name;
                                  }
                                  isError = false;
                                },
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                style: Utility.subtitleBlack_14(context),
                                decoration: Utility.editBoxInputDecoration(
                                    context,
                                    ConstantString.enter_contractor_name,
                                    ConstantString.contractor_name,
                                    contractorNameFocus.hasFocus),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 10.0),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: TextFormField(
                                autofocus: false,
                                showCursor: true,
                                controller: contractorMobileNoController,
                                onChanged: (value) {
                                  if (isError) {
                                    _formKey?.currentState!.validate();
                                  }
                                },
                                focusNode: contractorMobileNoFocus,
                                validator: (val) {
                                  if (val == null || val.isEmpty) {
                                    isError = true;
                                    return ConstantString
                                        .enter_contractor_number;
                                  }
                                  isError = false;
                                },
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                style: Utility.subtitleBlack_14(context),
                                decoration: Utility.editBoxInputDecoration(
                                    context,
                                    ConstantString.enter_contractor_number,
                                    ConstantString.contractor_mobile_no,
                                    contractorMobileNoFocus.hasFocus),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 10.0),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: TextFormField(
                                autofocus: false,
                                showCursor: true,
                                controller: noOfPainterController,
                                onChanged: (value) {
                                  if (isError) {
                                    _formKey?.currentState!.validate();
                                  }
                                },
                                focusNode: noOfPainterFocus,
                                validator: (val) {
                                  if (val == null || val.isEmpty) {
                                    isError = true;
                                    return ConstantString.enter_no_of_painter;
                                  }
                                  isError = false;
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'[0-9]')),
                                ],
                                textInputAction: TextInputAction.next,
                                maxLength: 10,
                                style: Utility.subtitleBlack_14(context),
                                decoration: Utility.editBoxInputDecoration(
                                    context,
                                    ConstantString.enter_no_of_painter,
                                    ConstantString.number_of_painter,
                                    noOfPainterFocus.hasFocus),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 10.0),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: firmNameController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: firmNameFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_firm_name;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context,
                              ConstantString.enter_firm_name,
                              ConstantString.firm_name,
                              firmNameFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 20.0),
                      Container(
                        color: ColorConstant.white,
                        height: 80,
                        child: Column(
                          children: [
                            Utility.uiSizeBox(0.0, 5.0),
                            Container(
                              margin: const EdgeInsets.only(
                                  left: 28.0, right: 28.0, top: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 44,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.update,
                                        style: Utility.buttonTextStyle_14(
                                            context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                      //if (_formKey!.currentState!.validate()) {
                                      FocusScope.of(context).unfocus();

                                      Navigator.of(context).pop();
                                      // }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 5.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future getImage(ImageSource source) async {
    XFile? image = await ImagePicker()
        .pickImage(source: source, maxHeight: 200.0, maxWidth: 200.0);

    if (image != null) {
      setState(() {
        _image = image;
      });
    }
  }
}
