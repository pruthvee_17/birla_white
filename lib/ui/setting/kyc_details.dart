import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/login/login_screen.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/services.dart';

class KYCDetails extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return KYCDetailsState();
  }
}

class KYCDetailsState extends State<KYCDetails> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();
  final TextEditingController identifyController = TextEditingController();
  final TextEditingController addressController = TextEditingController();

  final FocusNode identifyFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();
  bool isError = false;
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    identifyFocus.addListener(() {
      setState(() {});
    });
    addressFocus.addListener(() {
      setState(() {});
    });
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.kyc_details, context),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: ColorConstant.white,
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 20),
                  child: Column(
                    children: [

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: identifyController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: identifyFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_identity_proof;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context, ConstantString.enter_identity_proof, ConstantString.identity_proof+"*", identifyFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: addressController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: addressFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_address_proof;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context, ConstantString.enter_address_proof, ConstantString.address_proof+"*", addressFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 20.0),
                      Container(
                        color: ColorConstant.white,
                        height: 80,
                        child: Column(
                          children: [
                            Utility.uiSizeBox(0.0, 5.0),
                            Container(
                              margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 44,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.update, style: Utility.buttonTextStyle_14(context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                      //if (_formKey!.currentState!.validate()) {
                                      FocusScope.of(context).unfocus();

                                      Navigator.of(context).pop();
                                      // }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 5.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
