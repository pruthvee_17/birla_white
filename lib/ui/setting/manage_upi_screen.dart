import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/home/home_screen_widget.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ManageUpiScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ManageUpiScreenState();
  }
}

class ManageUpiScreenState extends State<ManageUpiScreen> {
  final GlobalKey<FormState>? _formKey = GlobalKey<FormState>();

  final TextEditingController preferredRetailerController =
      TextEditingController();

  final FocusNode preferredRetailerFocus = FocusNode();

  bool isError = false;
  bool term_condition = false;
  bool privacy_policy = false;
  var selectedWork;
  var businessWorkList = ['Individual', 'Government', 'Contractor'];
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: Utility.commonAppBar(ConstantString.manage_upi, context),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text(
              ConstantString.navHome,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.home),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navScan,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.qr_code),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navRedeem,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.card_giftcard),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            title: Text(
              ConstantString.navSetting,
              style: Utility.subtitleAppColor_14(context),
            ),
            icon: const Icon(Icons.settings),
            activeColor: ColorConstant.appColor,
            inactiveColor: ColorConstant.appDarkColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),
      body: Column(
        children: [
          Flexible(
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(
                    left: 10, right: 10, top: 10, bottom: 10),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Utility.uiSizeBox(0.0, 10.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autofocus: false,
                          showCursor: true,
                          controller: preferredRetailerController,
                          onChanged: (value) {
                            if (isError) {
                              _formKey?.currentState!.validate();
                            }
                          },
                          focusNode: preferredRetailerFocus,
                          validator: (val) {
                            if (val == null || val.isEmpty) {
                              isError = true;
                              return ConstantString.enter_upi;
                            }
                            isError = false;
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          style: Utility.subtitleBlack_14(context),
                          decoration: Utility.editBoxInputDecoration(
                              context,
                              ConstantString.enter_upi_details,
                              ConstantString.upi + "*",
                              preferredRetailerFocus.hasFocus),
                        ),
                      ),
                      Utility.uiSizeBox(0.0, 20.0),
                      Container(
                        color: ColorConstant.white,
                        height: 80,
                        child: Column(
                          children: [
                            Utility.uiSizeBox(0.0, 5.0),
                            Container(
                              margin: const EdgeInsets.only(
                                  left: 28.0, right: 28.0, top: 10),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 44,
                                  child: ElevatedButton(
                                    child: Text(ConstantString.update,
                                        style: Utility.buttonTextStyle_14(
                                            context)),
                                    style: Utility.buttonStyle(context),
                                    onPressed: () {
                                      //if (_formKey!.currentState!.validate()) {
                                      FocusScope.of(context).unfocus();

                                      Navigator.of(context).pop();
                                      // }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Utility.uiSizeBox(0.0, 5.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
