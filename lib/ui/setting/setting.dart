import 'dart:io';

import 'package:birla_white/common/color_constant.dart';
import 'package:birla_white/ui/setting/edit_bank_info_screen.dart';
import 'package:birla_white/ui/setting/edit_profile_screen.dart';
import 'package:birla_white/ui/setting/kyc_details.dart';
import 'package:birla_white/ui/setting/manage_upi_screen.dart';
import 'package:birla_white/utils/constant_string.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Setting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SettingState();
  }
}

class SettingState extends State<Setting> {
  XFile? _image;
  String imageUrl = "";
  double iconSize = 18;
  final TextEditingController nameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorConstant.grey.withOpacity(0.5),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  height: 120.0,
                  width: 120.0,
                  child: _image != null
                      ? CircleAvatar(
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.white,
                          backgroundImage: FileImage(File(_image!.path)),
                        )
                      : imageUrl != "" && imageUrl != "null" && imageUrl != null
                          ? CachedNetworkImage(
                              imageBuilder: (context, imageProvider) {
                                return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(50)),
                                    border: Border.all(color: Colors.grey, width: 1.5),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                );
                              },
                              imageUrl: imageUrl,
                              fit: BoxFit.cover,
                              placeholder: (context, url) {
                                return Image.asset(
                                  "assets/images/app_logo.png",
                                );
                              },
                              errorWidget: (context, url, error) {
                                return Image.asset(
                                  "assets/images/app_logo.png",
                                );
                              },
                            )
                          : CircleAvatar(
                              child: Text(
                                nameController.text.isNotEmpty ? nameController.text.toString().split("")[0] : "A",
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 30.0,
                                ),
                              ),
                              backgroundColor: Theme.of(context).primaryColor.withAlpha(900),
                            ),
                ),
                InkWell(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return SafeArea(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ListTile(
                                leading: const Icon(Icons.camera),
                                title: const Text('Camera'),
                                onTap: () {
                                  getImage(ImageSource.camera);
                                  Navigator.pop(context);
                                },
                              ),
                              ListTile(
                                leading: const Icon(Icons.image),
                                title: const Text('Gallery'),
                                onTap: () {
                                  getImage(ImageSource.gallery);
                                  // dismiss the modal sheet
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 100.0, left: 70.0),
                    width: 38.0,
                    height: 38.0,
                    decoration: BoxDecoration(color: Theme.of(context).primaryColor, borderRadius: const BorderRadius.all(Radius.circular(30.0))),
                    child: const Icon(
                      Icons.edit,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 160),
                    child: Text(
                      "Pruthvee Lucky Patel",
                      style: TextStyle(fontSize: 18),
                    )),
                Container(
                    margin: const EdgeInsets.only(top: 190),
                    child: Text(
                      "pruthvee@gmail.com",
                      style: TextStyle(fontSize: 14),
                    ))
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Card(
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                        InkWell(
                          onTap: (){   Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => EditProfileInfoScreen()),
                          );},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 14.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  color: ColorConstant.yellow_70.withOpacity(0.2),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset(
                                    "assets/images/person.png",
                                    height: iconSize,
                                    width: iconSize,
                                    color: ColorConstant.yellow_58,
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 18, left: 14.0, bottom: 18, right: 2.0),
                                child:  Text(
                                  ConstantString.edit_profile,
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Flexible(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Container(
                                      margin: const EdgeInsets.only(right: 8.0),
                                      padding: const EdgeInsets.only(top: 18, left: 8.0, bottom: 18, right: 2.0),
                                      child: const Icon(Icons.navigate_next)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: (){   Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => EditBankInfoScreen()),
                          );},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 14.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),

                                  color: ColorConstant.green,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset(
                                    "assets/images/bank.png",
                                    height: iconSize,
                                    width: iconSize,
                                    color: ColorConstant.dark_green,
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 18, left: 14.0, bottom: 18, right: 2.0),
                                child:  Text(
                                  ConstantString.manage_bank,
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Flexible(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Container(
                                      margin: const EdgeInsets.only(right: 8.0),
                                      padding: const EdgeInsets.only(top: 18, left: 8.0, bottom: 18, right: 2.0),
                                      child: const Icon(Icons.navigate_next)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: (){   Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ManageUpiScreen()),
                          );},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 14.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  color: ColorConstant.pink,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset(
                                    "assets/images/upi.png",
                                    height: iconSize,
                                    width: iconSize,
                                    //color: ColorConstant.dark_pink,
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 18, left: 14.0, bottom: 18, right: 2.0),
                                child:  Text(
                                  ConstantString.manage_upi,
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Flexible(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Container(
                                      margin: const EdgeInsets.only(right: 8.0),
                                      padding: const EdgeInsets.only(top: 18, left: 8.0, bottom: 18, right: 2.0),
                                      child: const Icon(Icons.navigate_next)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: (){  Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) {
                              return KYCDetails();
                            },
                          ));},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 14.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  color: ColorConstant.purple_70.withOpacity(0.2),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset(
                                    "assets/images/key.png",
                                    height: iconSize,
                                    width: iconSize,
                                    color: ColorConstant.purple_60,
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 18, left: 14.0, bottom: 18, right: 2.0),
                                child:  Text(
                                  ConstantString.kyc_details,
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Flexible(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Container(
                                      margin: const EdgeInsets.only(right: 8.0),
                                      padding: const EdgeInsets.only(top: 18, left: 8.0, bottom: 18, right: 2.0),
                                      child: const Icon(Icons.navigate_next)),
                                ),
                              ),
                            ],
                          ),
                        ),
                     /*   Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 14.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(6)),
                                color: ColorConstant.orange_70.withOpacity(0.5),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                  "assets/images/testimonial.png",
                                  height: iconSize,
                                  width: iconSize,
                                  color: ColorConstant.orange_58,
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 18, left: 14.0, bottom: 18, right: 2.0),
                              child:  Text(
                                ConstantString.submit_testimonials,
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Flexible(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                    margin: const EdgeInsets.only(right: 8.0),
                                    padding: const EdgeInsets.only(top: 18, left: 8.0, bottom: 18, right: 2.0),
                                    child: const Icon(Icons.navigate_next)),
                              ),
                            ),
                          ],
                        ),*/
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future getImage(ImageSource source) async {
    var image = await ImagePicker().pickImage(source: source, maxHeight: 200.0, maxWidth: 200.0);

    if (image != null) {
      setState(() {
        _image = image;
      });
    }
  }
}
