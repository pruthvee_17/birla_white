import 'dart:async';
import 'dart:io';

import 'package:birla_white/utils/constant_string.dart';
import 'package:birla_white/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class StoreLocatorScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StoreLocatorScreenState();
  }
}

class StoreLocatorScreenState extends State<StoreLocatorScreen> {
  var loading = true;
  WebViewController? _webViewController;
  final Completer<WebViewController> _controller = Completer<WebViewController>();

  void initState() {
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Utility.commonAppBar(ConstantString.drawerStoreLocator, context),
      body:  Stack(
        children: [
          WebView(
            initialUrl: ConstantString.store_locator_url,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _webViewController = webViewController;
              _controller.complete(webViewController);
            },
            onPageStarted: (url) {
              loading = true;
              setState(() {

              });


            },
            onProgress: (int progress) {
              print("WebView is loading (progress : $progress%)");
            },
            onPageFinished: (String url) {
              print('Page finished loading: $url');


              _webViewController!
                  .evaluateJavascript('document.getElementsByClassName("corp-menu")[0].remove()')
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint('$onError'));  _webViewController!
                  .evaluateJavascript('document.getElementsByClassName("heading-title")[0].remove();')
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint('$onError'));
              _webViewController!
                  .evaluateJavascript('document.getElementById("navigation").remove();')
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint('$onError'));
              _webViewController!
                  .evaluateJavascript('document.getElementsByClassName("write-to-us")[0].remove();')
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint('$onError'));
              _webViewController!
                  .evaluateJavascript('document.getElementsByTagName("footer")[0].remove();')
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint('$onError'));
              _webViewController!
                  .evaluateJavascript('document.getElementsByTagName("body")[0].style.marginTop = "0px";')
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint('$onError'));

              Future.delayed(const Duration(seconds: 1),(){
                loading = false;
                setState(() {

                });
              });
            },
          ),

          if(loading)Container(constraints: const BoxConstraints.expand(),color:Colors.white, child: const Center(child: CircularProgressIndicator()))
        ],
      ),
    );
  }
}
