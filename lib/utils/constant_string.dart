// ignore_for_file: non_constant_identifier_names

import 'package:flutter/services.dart';

class ConstantString {
//---------------------------------Shared Preferences String---------------------------//
  static String USER_ID = 'id';
  static String FIRST_NAME = 'first_name';
  static String LAST_NAME = 'last_name';
  static String GET_USER = 'get_user';
  static String USER = 'user';
  static String isLogin = 'islogin';

//---------------------------Bottom Navigation & Drawer String---------------------------//
  static String navHome = 'Home';
  static String navScan = 'Scan';
  static String navRedeem = 'Redeem';
  static String navSetting = 'Settings';
  static String navBackMsg = 'Press back one more time to exit from app';
  static String drawerAboutUs = 'About Us';
  static String drawerBlogs = 'Blogs';
  static String drawerHomeBuilder = 'Home Builder';
  static String drawerStoreLocator = 'Store Locator';
  static String drawerContactUs = 'Contact Us';
  static String drawerFAQ = 'FAQ';
  static String drawerNotification = 'Notifications';
  static String drawerLogout = 'Logout';
  static String drawerTestimonials = 'Testimonials';

  static String androidAPIKey = "AIzaSyA1bl9F8ujIJ9Y6_oGfcpNsVmJK623W0jo";

//------------------------------------common string --------------------------------------//


//======================Profile Info=========================//

  static String enter_mobile_no = 'Enter mobile no';
  static String mobile_no = 'Mobile No*';
  static String enter_password = 'Enter password';
  static String password = 'Password';
  static String enter_first_name = 'Enter first name';
  static String first_name = 'First Name*';
  static String enter_last_name = 'Enter last name';
  static String last_name = 'Last Name*';
  static String enter_email = 'Enter email';
  static String enter_valid_email = 'Enter valid email';
  static String email = 'Email';
  static String enter_firm_name = 'Enter firm name';
  static String firm_name = 'Firm Name*';
  static String enter_contractor_name = 'Enter contractor name';
  static String contractor_name = 'Contractor Name';
  static String enter_contractor_number = 'Enter contractor mobile no.';
  static String contractor_mobile_no = 'Contractor Mobile No.';
  static String enter_no_of_painter = 'Enter number of painter';
  static String number_of_painter = 'Number Of Painter';
  static String select_salutation = 'Select Salutation';
  static String applicator_category = 'Applicator Category*';
  static String select_applicator_category = 'Select Applicator Category';


  static String enter_full_name = 'Enter Full name';
  static String full_name = 'Full Name';
  static String address = 'Address';
  static String enter_address = 'Enter Address';
  static String enter_valid_address = 'Enter valid address';
  static String enter_profession = 'Enter Profession';
  static String profession = 'Profession';
  static String enter_query = 'Enter Your Message';
  static String your_query = 'Your Message';
  static String enter_project_subject = 'Enter Subject';
  static String project_subject = 'Subject';

//============================Address Info=============================//

  static String enter_address_line_1 = 'Enter address line 1';
  static String address_line_1 = 'Address Line 1*';
  static String enter_address_line_2 = 'Enter address line 2';
  static String address_line_2 = 'Address Line 2';
  static String enter_pincode = 'Enter pincode';
  static String pincode = 'Pincode*';
  static String enter_mother_name = 'Enter mother name';
  static String mother_name = 'Mother Name*';
  static String city = 'City*';
  static String enter_city = 'Enter city';
  static String state = 'State*';
  static String enter_state = 'Enter State';
  static String select_dob = 'Select DOB';
  static String dob = 'Date Of Birth*';
  static String select_dom = 'Select DOM';
  static String dom = 'DOM';

//=============================bank Info================================//


  static String enter_bank_ac_no = 'Enter bank a/c no';
  static String bank_ac_no = 'Bank Account No.';
  static String enter_bank_ac_name = 'Enter bank a/c name';
  static String bank_ac_name = 'Bank Account Name';
  static String enter_ifsc_code = 'Enter IFSC code';
  static String ifsc_code = 'IFSC Code';
  static String enter_upi_details = 'Enter UPI details';
  static String upi_details = 'UPI Details';
  static String enter_identity_proof = 'Enter identity proof';
  static String identity_proof = 'Identity Proof (PAN No.)';
  static String enter_address_proof = 'Enter address proof';
  static String address_proof = 'Address Proof (Aadhaar No.)';
  static String enter_preferred_retailer = 'Enter Preferred Retailer';
  static String preferred_retailer = 'Preferred Retailer*';

  static String enter_upi = 'Enter valid UPI ID';
  static String upi = 'UPI ID';
  static String verify_proceed = 'verify & proceed';

//======================Retailer Mobile Info=========================//

  static String enter_retailer_mobile_number = 'Enter retailer mobile number';
  static String mapped_retailer = 'Mapped retailer';

//======================Change Password Info=========================//

  static String enter_old_password = 'Enter old password';
  static String old_password = 'Old password';
  static String enter_new_password = 'Enter new password';
  static String new_password = 'New password';
  static String enter_confirm_password = 'Enter confirm password';
  static String confirm_password = 'Confirm password';


//============================MISC. Info==============================//

  static String business_work = 'Business work';
  static String select_business_work = 'Select Business Work';
  static String painter_type = 'Painter Type';
  static String select_painter_type = 'Select Painter Type';


//============================Label Info==============================//

  static String confirm = 'Confirm';
  static String resend_otp = ' RESEND OTP';
  static String dont_receive_the_otp = 'Don\'t receive the OTP ?';
  static String enter_coupon_code = 'Enter coupon code';
  static String coupon_code = 'Coupon code*';
  static String clear = 'Clear';
  static String code_history = 'Scanned Code History';
  static String notification_history = 'Notification History';
  static String manage_bank = 'Manage Bank';
  static String manage_upi = 'Manage UPI';
  static String submit_testimonials = 'Submit Testimonials';
  static String edit_profile = 'Edit Profile';
  static String your_points = 'Your Points';
  static String redeem = 'Redeem';
  static String point_history = 'Point History';
  static String products = 'Products';
  static String participating_products = 'Participating Products';
  static String category = 'Category';
  static String scan = 'Scan';
  static String status = 'Status';
  static String profile_information = 'Profile Information';
  static String address_information = 'Address Information';
  static String bank_details = 'Bank / KYC Details';
  static String misc_details = 'Misc. Details';
  static String salutation = 'Salutation';
  static String welcome_back = 'Welcome back';
  static String remember_me = 'Remember Me';
  static String term_condition = 'Terms & Conditions';
  static String privacy_policy = 'Privacy Policy';
  static String forgot_password = 'Forgot password';
  static String change_password = 'Change password';
  static String kyc_details = 'KYC Details';
  static String scan_code = 'Scan Now';
  static String login = 'Login';
  static String save_next = 'Save & Next';
  static String update = 'Update';
  static String back = 'Back';
  static String new_user = 'New user ? ';
  static String signup = 'Sign up';
  static String sale_team_login = 'Sales Team login';


//===============================errors String================================//

  static String enter_valid_mobile_no = 'Enter valid mobile no';
  static String enter_valid_preferred_retailer = 'Enter valid preferred retailer';
  static String password_too_short = 'Password too short.';

  static String submit = 'Submit';

  static String enter_the_6_digit_code_sent_to_you_at = "Enter the 6-digits code sent to you \nat ";

  static String hold_your_phone = "Hold your phone approx 10cm \n away from the code steady for 2 second";




//===============================Constant String=================================//

  static var store_locator_url="https://www.birlawhite.com/en/store-locator";
  static var term_condition_url="https://www.birlawhite.com/en/terms-and-conditions";
  static var privacy_policy_url="https://www.birlawhite.com/en/privacy-policy";
  static var redeem_url="https://edenred.co.in";
  static var birla_support_call="+91 1800 562 5223";
  static var birla_support_email= "suppport@birlawhite.com";

  static var pending= "pending";
  static var profile_completion= "Profile Completion";
  static String get_otp = 'GET OTP';
  static String verify = 'VERIFY';
  static String we_will_send_you_one_time_code_on_your_phone_number = "We will send you One Time Code \non your phone number ";
  static String mobile_verification = "Mobile Verification";
  static String register_here = 'Register Here';


}
