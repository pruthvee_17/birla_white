import 'package:birla_white/common/color_constant.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

import 'constant_string.dart';

class Utility {
  static String Tag = 'Utility';

  static void showToast(String string) {
    Fluttertoast.showToast(
      msg: string,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.grey,
      fontSize: 16,
      textColor: Colors.black,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
    );
  }

  static void printLog(String Tag, String string) {
    print(Tag + ': ' + string);
  }

  static bool validEmail(String email) {
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    return emailValid;
  }

  static Widget uiSizeBox(double width, double height) {
    return SizedBox(
      width: width,
      height: height,
    );
  }

  static TextStyle subtitleBoldGrey_11(BuildContext context) {
    return TextTheme.copyWith(
        color: ColorConstant.grey, fontWeight: FontWeight.bold, fontSize: 10.5);
  }

  static TextStyle subtitleBoldDarKGrey_11(BuildContext context) {
    return TextTheme.copyWith(
        color: ColorConstant.darkGrey,
        fontWeight: FontWeight.bold,
        fontSize: 10.5);
  }

  static TextStyle subtitleBlack_10(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.black,
      fontWeight: FontWeight.w400,
      fontSize: 10,
    );
  }

  static TextStyle subtitleWhite_12(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.white,
      fontWeight: FontWeight.w400,
      fontSize: 12,
    );
  }

  static TextStyle subtitleAppDarkColor_12(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.appDarkColor,
      fontWeight: FontWeight.w400,
      fontSize: 12,
    );
  }

  static TextStyle subtitleBlack_14(BuildContext context) {
    return TextTheme.copyWith(color: ColorConstant.black);
  }

  static TextStyle subtitleWhite_14(BuildContext context) {
    return TextTheme.copyWith(color: ColorConstant.white);
  }

  static TextStyle subtitleGreen_14(BuildContext context) {
    return TextTheme.copyWith(color: ColorConstant.status, fontSize: 14);
  }

  static TextStyle subtitleGrey_14(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.grey,
    );
  }

  static TextStyle subtitleDarkGrey_14(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.darkGrey,
    );
  }

  static TextStyle subtitleDarkGrey_12(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.darkGrey,
      fontSize: 12,
    );
  }

  static TextStyle subtitleBlack_16(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.black,
      fontSize: 16,
    );
  }

  static TextStyle subtitleBlack_18(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.black,
      fontSize: 18,
    );
  }

  static TextStyle subtitleBoldBlack_14(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.black,
      fontWeight: FontWeight.w500,
      fontSize: 14,
    );
  }

  static TextStyle subtitleBoldBlack_16(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.black,
      fontWeight: FontWeight.w700,
      fontSize: 16,
    );
  }

  static TextStyle subtitleWhite_16(BuildContext context) {
    return TextTheme.copyWith(fontSize: 16, color: ColorConstant.white);
  }

  static TextStyle subtitleAppDarkColor_14(BuildContext context) {
    return TextTheme.copyWith(color: ColorConstant.appDarkColor);
  }

  static TextStyle subtitleAppDarkColor_17(BuildContext context) {
    return TextTheme.copyWith(color: ColorConstant.appDarkColor, fontSize: 20);
  }

  static TextStyle subtitleAppColor_14(BuildContext context) {
    return TextTheme.copyWith(
      fontWeight: FontWeight.w400,
    );
  }

  static TextStyle subtitleWhiteBold_24(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.white,
      fontWeight: FontWeight.w500,
      fontSize: 24,
    );
  }

  static TextStyle subtitleAppColorBold_24(BuildContext context) {
    return TextTheme.copyWith(
      //color: ColorConstant.appColor,
      fontWeight: FontWeight.w500,
      fontSize: 24,
    );
  }

  static TextStyle subtitleUnderlineAppColor_14(BuildContext context) {
    return TextTheme.copyWith(
      // color: ColorConstant.appColor,
      fontWeight: FontWeight.w400,
      decoration: TextDecoration.underline,
    );
  }

  static TextStyle buttonTextStyle_18(BuildContext context) {
    return ButtonTheme;
  }

  static TextStyle buttonTextStyle_14(BuildContext context) {
    return ButtonTheme.copyWith(
      fontSize: 14,
    );
  }

  static TextStyle ButtonTheme = TextStyle(
    color: ColorConstant.white,
    fontWeight: FontWeight.w500,
    letterSpacing: 1,
    fontSize: 16,
  );

  static BoxShadow commonCardShadow = BoxShadow(
    color: Colors.grey.withOpacity(0.5),
    spreadRadius: 1,
    blurRadius: 8,
    offset: Offset(0, 0),
  );

  static TextStyle TextTheme =
      TextStyle(color: ColorConstant.appColor, fontSize: 14);

  static TextStyle headlineGrey_17(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .headline6!
        .copyWith(fontSize: 17, color: ColorConstant.grey);
  }

  static TextStyle headlineBlack_17(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .headline6!
        .copyWith(fontSize: 17, color: ColorConstant.black);
  }

  static ButtonStyle buttonStyle(BuildContext context) {
    return ButtonStyle(
      backgroundColor: MaterialStateProperty.all(ColorConstant.appDarkColor),
      //Background Color
      elevation: MaterialStateProperty.all(8),
      //Defines Elevation
      shadowColor: MaterialStateProperty.all(
          ColorConstant.appDarkColor), //Defines shadowColor
    );
  }

  static Future<void> makeCall() async {
    //String mobile = await AppSharedPreferences.getUserMobile();
    //if (mobile != null && mobile.isNotEmpty) {
    Utility.makeURL(
        "tel:" + ConstantString.birla_support_call.replaceAll(" ", ""));
    //} else {
    // Utility.showToast("data not found");
    //}
  }

  static Future<void> writeEmail() async {
    // String email = await AppSharedPreferences.getUserEmail();
    // if (email != null && email.isNotEmpty) {
    Utility.makeURL("mailto:" + ConstantString.birla_support_email);
    //} else {
    //Utility.showToast("data not found");
    // }
  }

  static Future<void> makeURL(String url) async {
    Utility.printLog(Tag, url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static TextStyle subtitleUnderlineAppDarkColor_18(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.appColor,
      fontWeight: FontWeight.w800,
      fontSize: 16,
      decoration: TextDecoration.underline,
    );
  }

  static TextStyle subtitleAppColor_25(BuildContext context) {
    return TextTheme.copyWith(fontWeight: FontWeight.w400, fontSize: 25.0);
  }

  static TextStyle subtitleAppDarkColor_15(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.appDarkColor,
      fontWeight: FontWeight.w600,
      fontSize: 15,
    );
  }

  static TextStyle subtitleYellow_10(BuildContext context) {
    return TextTheme.copyWith(
      color: ColorConstant.yellow,
      fontSize: 10,
    );
  }

  static TextStyle headlineGrey_14(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .headline6!
        .copyWith(fontSize: 14, color: ColorConstant.grey);
  }

  static AppBar commonAppBar(String title, BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        title,
        style: Utility.subtitleWhite_16(context),
      ),
    );
  }

  static InputDecoration editBoxInputDecoration(
      BuildContext context, String hintText, String labelText, bool hasFocus,
      {IconButton? iconButton}) {
    return InputDecoration(
      fillColor: ColorConstant.darkGrey,
      hintText: hintText,
      labelText: labelText,
      focusColor: ColorConstant.appColor,
      counterText: '',
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: ColorConstant.appColor,
        ),
      ),
      labelStyle: Theme.of(context).textTheme.subtitle1!.copyWith(
            fontSize: 15,
            color: hasFocus ? ColorConstant.appColor : ColorConstant.darkGrey,
          ),
      hintStyle: Theme.of(context).textTheme.subtitle1!.copyWith(
            fontSize: 14,
            color: Colors.black,
          ),
        suffixIcon: iconButton,
    );
  }

  static InputDecoration editBoxPasswordInputDecoration(
      BuildContext context,
      String hintText,
      String labelText,
      bool passwordVisible,
      Function() toggleObscured,
      bool hasFocus) {
    return InputDecoration(
      fillColor: ColorConstant.darkGrey,
      hintText: hintText,
      labelText: labelText,
      counterText: '',
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: ColorConstant.appColor,
        ),
      ),
      labelStyle: TextTheme.copyWith(
        fontSize: 15,
        color: hasFocus ? ColorConstant.appColor : ColorConstant.darkGrey,
      ),
      hintStyle: TextTheme.copyWith(
        fontSize: 14,
        color: Colors.black,
      ),
      suffixIcon: Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
        child: GestureDetector(
          onTap: toggleObscured,
          child: Icon(
            passwordVisible
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined,
            size: 20,
          ),
        ),
      ),
    );
  }

  static InputDecoration dropDownInputDecoration(List<String> salutationItems) {
    return InputDecoration(
      enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorConstant.darkGrey, width: 1)),
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorConstant.appColor, width: 1)),
    );
  }
}

class CurvePainterLight extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = ColorConstant.appLightColor.withOpacity(0.4);
    paint.style = PaintingStyle.fill; // Change this to fill

    var path = Path();

    path.moveTo(0, size.height * 0.76);
    path.quadraticBezierTo(
        size.width / 2, size.height / 0.64, size.width, size.height * 0.85);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CurvePainterDark extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = ColorConstant.appDarkColor;
    paint.style = PaintingStyle.fill; // Change this to fill

    var path = Path();

    path.moveTo(0, size.height * 0.84);
    path.quadraticBezierTo(
        size.width / 2, size.height / 0.74, size.width, size.height * 0.8);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CirclePainter extends CustomPainter {
  final Paint lightBluePaint = Paint()
    ..color = ColorConstant.appLightColor.withOpacity(0.4)
    ..strokeWidth = 70.0
    ..style = PaintingStyle.stroke
    ..strokeJoin = StrokeJoin.round;

  @override
  void paint(Canvas canvas, Size size) {
    var path = Path();

    path.addOval(Rect.fromCircle(
      center: Offset(0, 0),
      radius: 80.0,
    ));
    canvas.drawPath(path, lightBluePaint);
  }

  /* @override
  void paint(Canvas canvas, Size size) {
    var rect = Rect.fromLTRB(
        -100, size.height - 20, size.width * 0.1, size.height + 50);

    final Path smallCircle = Path()..addOval(rect.inflate(50));
    canvas.drawPath(smallCircle, lightBluePaint);
  }
*/
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
